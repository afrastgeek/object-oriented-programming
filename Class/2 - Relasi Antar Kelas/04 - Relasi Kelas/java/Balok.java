public class Balok {
  private int panjang; //panjang balok
  private int lebar; //lebar balok
  private int tinggi; //tinggi balok

  Balok() {
    //konstruktor kosong
  }

  Balok(int panjang, int lebar, int tinggi) {
    //konstruktor langsung isi atribut
    this.panjang = panjang;
    this.lebar = lebar;
    this.tinggi = tinggi;
  }

  public void setPanjang(int panjang) {
    this.panjang = panjang;
  }

  public void setLebar(int lebar) {
    this.lebar = lebar;
  }

  public void setTinggi(int tinggi) {
    this.tinggi = tinggi;
  }

  public int getPanjang() {
    return this.panjang;
  }

  public int getLebar() {
    return this.lebar;
  }

  public int getTinggi() {
    return this.tinggi;
  }

  public int volume() {
    return (panjang * lebar * tinggi); //menghitung volume balok
  }

  public int luas() {
    return ((2 * panjang * lebar) + (2 * panjang * tinggi) + (2 * lebar * tinggi));
  }
}
