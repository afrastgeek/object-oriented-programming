<?php
/**
 * Configuration
 *
 * PHP version 7.1.4
 *
 * @author     Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 * @copyright  2009 Rosa Ariani Sukamto
 */

/**
 * Database Configuration
 */
$db_host = "127.0.0.1";
$db_user = "root";
$db_password = "";
$db_name = "penyewaan_vcd";
