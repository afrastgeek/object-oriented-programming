<?php
/**
 * DB Class for Accessing Database
 *
 * PHP version 7.1.4
 *
 * @package    App
 * @author     Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 * @copyright  2009 Rosa Ariani Sukamto
 */
namespace App\Core;

/**
 * Class for db.
 */
class DB
{
    public $db_host = '';
    public $db_user = '';
    public $db_password = '';
    public $db_name = '';
    public $db_link = '';
    public $result = 0;

    /**
     * Class Constructor
     *
     * @param      string  $db_host      The database host
     * @param      string  $db_user      The database user
     * @param      string  $db_password  The database password
     * @param      string  $db_name      The database name
     */
    public function __construct(
        $db_host = '',
        $db_user = '',
        $db_password = '',
        $db_name = ''
    ) {
        $this->db_host = $db_host;
        $this->db_user = $db_user;
        $this->db_password = $db_password;
        $this->db_name = $db_name;
    }

    /**
     * Open Connection to Database
     */
    public function open()
    {
        $this->db_link = mysqli_connect(
            $this->db_host,
            $this->db_user,
            $this->db_password,
            $this->db_name
        );
    }

    /**
     * Execute SQL Query
     *
     * @param      string  $query  The query
     *
     * @return     bool    Status from executed query
     */
    public function execute($query = "")
    {
        $this->result = mysqli_query($this->db_link, $query);
        return $this->result;
    }

    /**
     * Gets the result.
     *
     * @return     array  The result for each row in the database.
     */
    public function getResult()
    {
        return mysqli_fetch_row($this->result);
    }

    /**
     * Close Database Connection
     */
    public function close()
    {
        mysqli_close($this->db_link);
    }
}
