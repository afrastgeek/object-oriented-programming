<?php
/**
 * Class for Accessing Telephone Table
 *
 * PHP version 7.1.4
 *
 * @package    App
 * @author     Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 * @copyright  2009 Rosa Ariani Sukamto
 */
namespace App;

use App\Core\DB;

class Telepon extends DB
{
    /**
     * Gets the telepon lists.
     *
     * @return     bool  Status of query execution.
     */
    public function getTelepon()
    {
        $query = "SELECT * FROM telepon";
        return $this->execute($query);
    }
}
