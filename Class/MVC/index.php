<?php
/**
 * Application Entry Point
 *
 * PHP version 7.1.4
 *
 * @package    App
 * @author     Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 * @copyright  2009 Rosa Ariani Sukamto
 */

require __DIR__.'/bootstrap/autoload.php';
include("conf.php");

use App\Telepon;
use App\Core\Template;

// get all telephone data
$telepon = new Telepon($db_host, $db_user, $db_password, $db_name);
$telepon->open();
$telepon->getTelepon();

$data = '';

while (list($kode_anggota, $no_telepon) = $telepon->getResult()) {
    $data .= "
                <tr>
                    <td>".$kode_anggota."</td>
                    <td>".$no_telepon."</td>
                </tr>
             ";
}
$telepon->close();

// read template file
$template = new Template("templates/skin.html");

// insert content to template
$template->replace("DATA_TABEL", $data);

// write to stdout
$template->write();
