<?php
/**
 * Halo.php
 *
 * @author M Ammar Fadhlur Rahman <afrastgeek@gmail.com>
 */

/**
 * Write Hello World text
 *
 * Initialize a string with Hello World text, then write to the stdout.
 *
 * PHP version 4
 *
 * NOTE: Methods with the same name as their class will not be constructors in
 * a future version of PHP; Halo has a deprecated constructor as in PHP 7
 * because using PHP 4 style constructors.
 *
 * @author Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 */
class Halo
{

    /**
     * Variable to store Hello World phrase.
     *
     * @var public
     */
    public $kata = '';


    /**
     * Halo function in which used as constructor for Halo Class.
     */
    public function Halo()
    {
        $this->kata = 'Halo Dunia\n';

    }//end Halo()


    /**
     * Write defined Hello World phrase to stdout.
     */
    public function tulis()
    {
        echo $this->kata;

    }//end tulis()


}//end class
