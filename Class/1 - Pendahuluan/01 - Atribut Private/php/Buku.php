<?php

class Buku
{
    public $judul = "";
    public $pengarang = "";

    public function Buku($j="", $p="")
    {
        $this->judul = $j;
        $this->pengarang = $p;
    }

    public function setJudul($j="")
    {
        $this->judul = $j;
    }

    public function setPengarang($p="")
    {
        $this->pengarang = $p;
    }

    public function getJudul()
    {
        return $this->judul;
    }
 
    public function getPengarang()
    {
        return $this->pengarang;
    }
}
