<?php
/**
 * Titik.php
 *
 * @author M Ammar Fadhlur Rahman <afrastgeek@gmail.com>
 */

/**
 * Store coordinate value
 *
 * Easily store coordinate value in variable by using this class. Provided
 * function to store and get the value of x axis and y axis coordinate.
 *
 * @author Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 */
class Titik
{
    // Kelas yang digunakan untuk mepngimplementasikan sebuah tipe titik.
    // Koordinat x.
    private $_x;
    // Koordinat y.
    private $_y;


    /**
     * Construct each point in coordinate variable by zero.
     */
    public function __construct()
    {
        // Konstruktor.
        $this->x = 0;
        $this->y = 0;

    }//end __construct()


    /**
     * Sets the x.
     *
     * @param      int   $xp     x axis coordinate value
     */
    public function setX($xp)
    {
        // Mengeset nilai koordinat x.
        $this->x = $xp;

    }//end setX()


    /**
     * Gets the x.
     *
     * @return     int  The x coordinate value.
     */
    public function getX()
    {
        // Mengembalikan nilai koordinat x.
        return $this->x;

    }//end getX()


    /**
     * Sets the y.
     *
     * @param      int   $yp     Y axis coordinate value
     */
    public function setY($yp)
    {
        // Mengeset nilai koordinat y.
        $this->y = $yp;

    }//end setY()


    /**
     * Gets the y.
     *
     * @return     int  The y coordinate value.
     */
    public function getY()
    {
        // Mengembalikan nilai koordinat y.
        return $this->y;

    }//end getY()


    /**
     * Class Destructor
     */
    public function __destruct()
    {
        // Destruktor.

    }//end __destruct()


}//end class
