<?php
/**
 * index.php
 *
 * @author Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 * @author M Ammar Fadhlur Rahman <afrastgeek@gmail.com>
 */
include "Titik.php";

/* main untuk mengetes kelas Titik */
$t1 = new Titik();

$t1->setX(18);
$t1->setY(28);

echo "t1 : nilai X : " . $t1->getX() . "\n";
echo "t1 : nilai Y : " . $t1->getY() . "\n";
