class Titik {
  /* kelas yang digunakan untuk mengimplementasikan sebuah tipe titik */

  private int x; /* koordinat x */
  private int y; /* koordinat y */

  Titik() {
    /* konstruktor */
    x = 0;
    y = 0;
  }

  Titik(int xp, int yp) {
    /* konstruktor */
    x = xp;
    y = yp;
  }

  public void setX(int xp) {
    /* mengeset nilai koordinat x */
    x = xp;
  }

  public int getX() {
    /* mengembalikan nilai koordinat x */
    return x;
  }
  
  public void setY(int yp) {
    /* mengeset nilai koordinat y */
    y = yp;
  }

  public int getY() {
    /* mengembalikan nilai koordinat y */
    return y;
  }

  public void finalize() {
    /* destruktor */
  }
}

class Main {
  /* metode main untuk mengetes kelas Titik */

  public static void main(String[] args) {
    Titik t1 = new Titik();
    Titik t2 = new Titik(11, 9);

    t1.setX(18);
    t1.setY(28);

    System.out.println("t1 : nilai X : " + t1.getX());
    System.out.println("t1 : nilai Y : " + t1.getY());

    System.out.println("t2 : nilai X : " + t2.getX());
    System.out.println("t2 : nilai Y : " + t2.getY());
  }
}
