<?php
/**
 * Application Entry Point
 *
 * PHP version 7.1.4
 *
 * @package    App
 * @author     Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 * @copyright  2009 Rosa Ariani Sukamto
 */

require __DIR__.'/bootstrap/autoload.php';
require __DIR__.'/conf.php';

use App\Book;
use App\Customer;
use App\Core\Template;

/**
 * Form Handler
 */
if (isset($_POST['kode_buku'])) {
    $book = new Book($db_host, $db_user, $db_password, $db_name);
    $book->open();
    $book->add(
        $_POST['kode_buku'],
        $_POST['nama'],
        $_POST['penerbit'],
        $_POST['tahun']
    );
    $book->close();
}
if (isset($_POST['kode_pelanggan'])) {
    $customer = new Customer($db_host, $db_user, $db_password, $db_name);
    $customer->open();
    $customer->add(
        $_POST['kode_pelanggan'],
        $_POST['nama'],
        $_POST['noktp']
    );
    $customer->close();
}

/**
 * Get all book data
 */
$book = new Book($db_host, $db_user, $db_password, $db_name);
$book->open();
$book->all();

// convert book information in table row format
$book_in_row = '';
while (list($kode, $nama, $penerbit, $tahun) = $book->getResult()) {
    $book_in_row .= "<tr>";
    $book_in_row .= "<td>".$kode."</td>";
    $book_in_row .= "<td>".$nama."</td>";
    $book_in_row .= "<td>".$penerbit."</td>";
    $book_in_row .= "<td>".$tahun."</td>";
    $book_in_row .= "</tr>";
}

// close db connection
$book->close();

/**
 * Get all customer data
 */
$customer = new Customer($db_host, $db_user, $db_password, $db_name);
$customer->open();
$customer->all();

// convert customer information in table row format
$customer_in_row = '';
while (list($kode, $nama, $noktp) = $customer->getResult()) {
    $customer_in_row .= "<tr>";
    $customer_in_row .= "<td>".$kode."</td>";
    $customer_in_row .= "<td>".$nama."</td>";
    $customer_in_row .= "<td>".$noktp."</td>";
    $customer_in_row .= "</tr>";
}

// close db connection
$customer->close();

/**
 * Templating
 */
// read template file
$template = new Template("templates/skin.html");

// insert content to template
if ($book_in_row == '') {
    $book_in_row = '<td>Tidak ada data</td>';
}
if ($customer_in_row == '') {
    $customer_in_row = '<td>Tidak ada data</td>';
}

$template->replace("DATA_TBUKU", $book_in_row);
$template->replace("DATA_TPEMBELI", $customer_in_row);

// write to stdout
$template->write();
