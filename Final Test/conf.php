<?php
/**
 * Configuration
 *
 * PHP version 7.1.4
 *
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 */

/**
 * Database Configuration
 */
$db_host = "127.0.0.1";
$db_user = "root";
$db_password = "";
$db_name = "toko_buku";
