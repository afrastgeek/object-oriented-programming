<?php
/**
 * Class for Accessing Book Table
 *
 * PHP version 7.1.4
 *
 * @package    App
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 */
namespace App;

use App\Core\DB;

class Book extends DB
{
    /**
     * Gets all books.
     *
     * @return     bool  Status of query execution.
     */
    public function all()
    {
        $query = "SELECT * FROM tbuku";
        return $this->execute($query);
    }

    public function add($kode, $nama, $penerbit, $tahun)
    {
        //metode insert data telepon
        $query = "INSERT INTO tbuku VALUES ('$kode', '$nama', '$penerbit', '$tahun')";
        return $this->execute($query);
    }
}
