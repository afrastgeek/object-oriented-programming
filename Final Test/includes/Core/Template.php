<?php
/**
 * Class for reading template file and inserting data.
 *
 * PHP version 7.1.4
 *
 * @package    App
 * @author     Rosa Ariani Sukamto <rosa_if_itb_01@yahoo.com>
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 * @copyright  2009 Rosa Ariani Sukamto
 */
namespace App\Core;

/**
 * Class for template.
 */
class Template
{
    public $filename = '';    //handle file
    public $content = '';     //handle isi file

    /**
     * Class Constructor
     *
     * @param      string  $filename  The filename
     */
    public function __construct($filename = '')
    {
        $this->filename = $filename;
        $this->content = implode('', @file($filename));
    }

    /**
     * Replace DATA_ component with empty string.
     */
    public function clear()
    {
        $this->content = preg_replace("/DATA_[A-Z|_|0-9]+/", "", $this->content);
    }

    /**
     * Print content to stdout.
     */
    public function write()
    {
        $this->clear();
        print $this->content;
    }

    /**
     * Gets the content.
     *
     * @return     string  The content.
     */
    public function getContent()
    {
        $this->clear();
        return $this->content;
    }

    /**
     * Content Processing
     *
     * Replace DATA_ component on template with value given.
     *
     * @param      string  $component  The component
     * @param      string  $value      The value
     */
    public function replace($component = '', $value = '')
    {
        if (is_int($value)) {
            $value = __toString($value);
        } elseif (is_float($value)) {
            $value = __toString($value);
        } elseif (is_array($value)) {
            $value = '';
            foreach ($value as $item) {
                $value .= $item. ' ';
            }
        }

        $this->content = preg_replace("/$component/", $value, $this->content);
    }
}
