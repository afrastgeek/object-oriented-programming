<?php
/**
 * Class for Accessing Customer Table
 *
 * PHP version 7.1.4
 *
 * @package    App
 * @author     M Ammar Fadhlur Rahman <mafr@student.upi.edu>
 */
namespace App;

use App\Core\DB;

class Customer extends DB
{
    /**
     * Gets all customer.
     *
     * @return     bool  Status of query execution.
     */
    public function all()
    {
        $query = "SELECT * FROM tpembeli";
        return $this->execute($query);
    }

    public function add($kode, $nama, $noktp)
    {
        //metode insert data telepon
        $query = "INSERT INTO tpembeli VALUES ('$kode', '$nama', '$noktp')";
        return $this->execute($query);
    }
}
