Simple PHP MVC
--------------

> UAS PBO 2017
> M Ammar Fadhlur Rahman

----------
Penjelasan
----------

Aplikasi dimulai dari file `index.php`.

File ini memanggil file `autoload.php` di folder `bootstrap` yang berguna untuk
menerjemahkan class yang diperlukan (lihat statement `use`) menjadi perintah
require di php.

Kemudian memeriksa apakah ada request yang dikirim dari form atau tidak.
Jika ada, insert ke db.

Selanjutnya mengambil semua data yang ada di tabel `tbuku` dan tabel `tpembeli`.

Data yang didapat diolah ke dalam format table row.

Terakhir, data di terjemahkan ke dalam template dan ditampilkan ke stdout.



------------
Pola program
------------

```
index.php
 |
 |-> bootstrap/autoload.php
 |
 |
 |-> includes/Book.php      |
 |                          | --EXTEND--> includes/Core/DB.php
 |-> includes/Customer.php  |
 |
 |-> includes/Template.php

```

----------
Screenshot
----------
![Program ](screenshot.png)
