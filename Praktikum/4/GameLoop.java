import entity.Ball;
import global.Constant;
//import class from other file
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

public class GameLoop extends Application{

  public static void main(String[] args){
    Application.launch(args);
  }

  @Override
  public void start(Stage stage){
    Group root = new Group();
    Scene scene = new Scene(root, Constant.WIDTH, Constant.HEIGHT, Color.LIGHTBLUE);

    Ball bola = new Ball(root);
    bola.setRadius(25);     //diameter
    bola.setPosition(0,0);  //posisi
    bola.setVelocity(2);    //kecepatan

    Ball bola2 = new Ball(root);
    bola2.setRadius(25);     //diameter
    bola2.setPosition(0,0);  //posisi
    bola2.setVelocity(2);    //kecepatan

    Ball bola3 = new Ball(root);
    bola3.setRadius(25);     //diameter
    bola3.setPosition(0,0);  //posisi
    bola3.setVelocity(2);    //kecepatan

    Duration fps = Duration.millis(1000/60);
    KeyFrame oneFrame = new KeyFrame(fps, new EventHandler(){
      @Override
      public void handle(Event arg0) {
        /* perpindahan */
        bola.setPosition(bola.getX() + bola.getVelocity(), 0);
        bola2.setPosition(0, bola2.getY() + bola2.getVelocity());
        bola3.setPosition(bola3.getX() + bola3.getVelocity(), bola3.getY() + bola3.getVelocity());

        /* pengecekan untuk kembali ke posisi awal */
        if(bola.getX() > Constant.WIDTH) {
          bola.setPosition(0,0);
        }
        if(bola2.getY() > Constant.HEIGHT) {
          bola2.setPosition(0,0);
        }
        if(bola3.getY() > Constant.HEIGHT) {
          bola3.setPosition(0,0);
        }
      }
    });

    stage.setScene(scene);
    stage.show();

    Timeline t1 = new Timeline();
    t1.setCycleCount(Animation.INDEFINITE);
    t1.getKeyFrames().add(oneFrame);
    t1.play();
  }
}
