import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import entity.BolaJagoan;
import global.Constant;

public class GameLoop extends Application {
  private BolaJagoan bola;
  // variabel untuk menentukan kapan kemunculan peluru
  private int TIME = 0;

  public static void main(String[] args) {
    Application.launch(args);
  }

  @Override
  public void start(Stage stage) {
    stage.setTitle(Constant.TITLE + "v" + Constant.VERSION);
    Group root = new Group();
    Scene scene = new Scene(root, Constant.WIDTH, Constant.HEIGHT, Color.WHITE);

    // membuat bola jagoan
    bola = new BolaJagoan();
    bola.setWidth(50);
    bola.setPosition(0, Constant.HEIGHT - (bola.getRadius() * 2));
    bola.setVelocity(1);
    bola.setCursor(Cursor.HAND);
    bola.setOnMouseDragged(draggedCircle);
    bola.addToGroup(root);

    // untuk animasi
    KeyFrame oneFrame = new KeyFrame(Constant.FPS, new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        if (TIME % 30 == 0) {
          // untuk memunculkan peluru
          bola.shoot(root);
          bola.shootKiri(root);
          bola.shootKanan(root);
        }
        // untuk menggerakan peluru
        bola.updatePeluru(root);
        bola.updatePeluruKiri(root);
        bola.updatePeluruKanan(root);
        TIME++;
      }
    });

    stage.setScene(scene);
    stage.show();

    Timeline tl = new Timeline();
    tl.setCycleCount(Animation.INDEFINITE);
    tl.getKeyFrames().add(oneFrame);
    tl.play();
  }

  // untuk menggerakan bolajagoan dengan mouse
  EventHandler<MouseEvent> draggedCircle = new EventHandler<MouseEvent>(){
    @Override
    public void handle(MouseEvent mouse) {
      if (mouse.getSceneX() >= 0 && mouse.getSceneX()<Constant.WIDTH) {
        // untuk merubah
        bola.setPosition(mouse.getSceneX()-bola.getRadius(),mouse.getSceneY()-bola.getRadius());
      }
    }
  };
}
