import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {
  // inner class yang digunakan untuk mendefinisikan data yang ada di tabel
  public static class BarangBeli {
    //jenis datanya
    private final SimpleStringProperty No;
    private final SimpleStringProperty KodeBarang;
    private final SimpleStringProperty JenisBarang;
    private final SimpleStringProperty QtyBarang;
    private final SimpleStringProperty HargaBarang;

    private BarangBeli(String NO, String KB, String JB, String QB, String HB) {
      this.No = new SimpleStringProperty(NO);
      this.KodeBarang = new SimpleStringProperty(KB);
      this.JenisBarang = new SimpleStringProperty(JB);
      this.QtyBarang = new SimpleStringProperty(QB);
      this.HargaBarang = new SimpleStringProperty(HB);
    }

    // fungsi untuk mendapatkan no
    public String getNo() {
      return No.get();
    }

    public void setNo(String NO) {
      No.set(NO);
    }

    // fungsi untuk mendapatkan kode barang
    public String getKodeBarang() {
      return KodeBarang.get();
    }

    public void setKodeBarang(String KB) {
      KodeBarang.set(KB);
    }

    // fungsi untuk mendapatkan jenis barang
    public String getJenisBarang() {
      return JenisBarang.get();
    }

    public void setJenisBarang(String JB) {
      JenisBarang.set(JB);
    }

    // fungsi untuk mendapatkan no
    public String getQtyBarang() {
      return QtyBarang.get();
    }

    public void setQtyBarang(String QB) {
      QtyBarang.set(QB);
    }

    // fungsi untuk mendapatkan no
    public String getHargaBarang() {
      return HargaBarang.get();
    }

    public void setHargaBarang(String HB) {
      HargaBarang.set(HB);
    }
  }

  // mendeklarasikan tabel
  private TableView<BarangBeli> table = new TableView<BarangBeli>();
  // list yang digunakan untuk menyimpan data pada tabel
  private final ObservableList<BarangBeli> data2 =
    FXCollections.observableArrayList(
      new BarangBeli("1", "BRG001", "Makanan", "1", "400000")
    );
  private HBox hb = new HBox();

  public static void main(String[] args) {
    launch(args);
  }

  int no = 1;
  @Override
  public void start(Stage stage) {
    Scene scene = new Scene(new Group());
    Group root = new Group();

    stage.setTitle("Tabel");
    stage.setWidth(450);
    stage.setHeight(590);

    final Label label = new Label("Tabel Pembeli Barang");

    // pembuatan kolom pada tabel
    // kolom nomor
    TableColumn NoCol = new TableColumn("No");
    NoCol.setMaxWidth(50);
    // mengintegrasikan kolom pada tabel dengan data
    NoCol.setCellValueFactory(
      new PropertyValueFactory<BarangBeli, String>("No")
    );

    // kolom kode barang
    TableColumn kodebarangCol = new TableColumn("Kode Barang");
    kodebarangCol.setMaxWidth(120);
    kodebarangCol.setCellValueFactory(
      new PropertyValueFactory<BarangBeli, String>("KodeBarang")
    );

    // kolom jenis barang
    TableColumn jenisbarangCol = new TableColumn("Jenis Barang");
    jenisbarangCol.setMaxWidth(120);
    jenisbarangCol.setCellValueFactory(
      new PropertyValueFactory<BarangBeli, String>("JenisBarang")
    );

    TableColumn qtyCol = new TableColumn("Quantity");
    qtyCol.setMaxWidth(120);
    qtyCol.setCellValueFactory(
      new PropertyValueFactory<BarangBeli, String>("QtyBarang")
    );

    TableColumn hargaCol = new TableColumn("Harga");
    hargaCol.setMaxWidth(120);
    hargaCol.setCellValueFactory(
      new PropertyValueFactory<BarangBeli, String>("HargaBarang")
    );

    // add kolom ke tabel
    table.getColumns().addAll(NoCol, kodebarangCol, jenisbarangCol, qtyCol, hargaCol);
    //mengisi tabel dengan data
    table.setItems(data2);

    final VBox vbox = new VBox();
    vbox.setSpacing(5);
    vbox.setLayoutY(50);
    vbox.getChildren().addAll(label, table, hb);
    vbox.setPadding(new Insets(10, 0, 0, 10));

    // layout yang digunakan untuk menambah barang
    // untuk add kode barang
    final TextField addKodeBarang = new TextField();
    addKodeBarang.setPromptText("Kode Barang");
    addKodeBarang.setMaxWidth(kodebarangCol.getPrefWidth());

    // untuk add kuantiti barang
    final TextField addQtyBarang = new TextField();
    addQtyBarang.setPromptText("Qty Barang");
    addQtyBarang.setMaxWidth(qtyCol.getPrefWidth());

    ObservableList<String> options =
      FXCollections.observableArrayList(
        "Elektronik",
        "Makanan",
        "Minuman",
        "Lain-Lain"
      );
    final ComboBox addJenisBarang = new ComboBox(options);

    final TextField addHargaBarang = new TextField();
    addHargaBarang.setPromptText("Harga Barang");
    addHargaBarang.setMaxWidth(hargaCol.getPrefWidth());

    final Label label1 = new Label("");
    label1.setLayoutX(30);
    label1.setLayoutY(550);

    final Label label2 = new Label("");
    label1.setLayoutX(60);
    label1.setLayoutY(550);

    final Label label3 = new Label("");
    label1.setLayoutX(90);
    label1.setLayoutY(550);

    root.getChildren().addAll(label1, label2, label3);

    // untuk button tambah
    final Button addButton = new Button("Tambah");
    addButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        no++;
        // add data ke list
        data2.add(new BarangBeli(
                    "" + no,
                    addKodeBarang.getText(),
                    addJenisBarang.getValue().toString(),
                    addQtyBarang.getText(),
                    addHargaBarang.getText())
                 );
        addKodeBarang.setText("");
        addQtyBarang.setText("");
        addHargaBarang.setText("");
        label1.setText(addQtyBarang.getText());
        label1.setText(addHargaBarang.getText());
      }
    });

    // add ke scene
    hb.getChildren().addAll(addKodeBarang, addJenisBarang, addQtyBarang, addHargaBarang, addButton);
    hb.setSpacing(3);

    ((Group) scene.getRoot()).getChildren().addAll(vbox);

    stage.setScene(scene);
    stage.show();
  }
}
