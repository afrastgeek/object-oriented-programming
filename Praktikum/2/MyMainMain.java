import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.Group;
import javafx.scene.image.Image; //Untuk menampung gambar
import javafx.scene.image.ImageView; // Untuk menampilkan gambar
import javafx.stage.Stage;
import java.io.File;

public class MyMainMain extends Application {

  public static void main(String[] args) {
    Application.launch(args);
  }

  @Override public void start(Stage stage) {
    /**
     * SCENE 1
     * Welcome scene
     */
    Group root = new Group();
    Scene welcomeScene = new Scene(root, 300, 200); //Ukuran scene

    Text scenetitle = new Text("Cari Gambar Galaksi"); //scene title, untuk judul di scene
    scenetitle.setFont(Font.font("Calibri", FontWeight.BOLD, 30)); // mengatur font dari scene title
    scenetitle.setLayoutX(20); //posisi X
    scenetitle.setLayoutY(30); //posisi Y
    root.getChildren().add(scenetitle);

    Label lGalaxy = new Label("Galaksi"); //label galaksi
    lGalaxy.setLayoutX(30); //posisi X
    lGalaxy.setLayoutY(80); //posisi Y
    root.getChildren().add(lGalaxy);

    TextField tfGalaxy = new TextField(); //Textfield galaksi
    tfGalaxy.setLayoutX(100); //posisi X
    tfGalaxy.setLayoutY(80); //posisi Y
    root.getChildren().add(tfGalaxy);

    /*Tombol cari*/
    Button bCari = new Button("Cari"); //tombol cari
    bCari.setLayoutX(205); //posisi X
    bCari.setLayoutY(110); //posisi Y
    root.getChildren().add(bCari);

    /**
     * SCENE 2
     * Main scene
     */
    Group rootM = new Group();
    Scene mainScene = new Scene(rootM, 400, 450);

    //tampungan gambar
    ImageView ViewM = new ImageView();
    ViewM.setFitHeight(400); //untuk ukuran panjang
    ViewM.setFitWidth(400); //untuk ukuran lebar

    Label mNama = new Label("Gambar tidak ditemukan!");
    mNama.setLayoutX(10);
    mNama.setLayoutY(415);

    Button mBack = new Button("Cari yang lain!");
    mBack.setLayoutX(300);
    mBack.setLayoutY(412);

    rootM.getChildren().add(mNama);
    rootM.getChildren().add(mBack);
    rootM.getChildren().add(ViewM);

    //tombol login yang ada di scene welcomeScene
    bCari.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        String galaxy = tfGalaxy.getText();
        Image img = null;
        try {
          img = new Image("img/" + galaxy + ".jpg");
          mNama.setText(galaxy + " Galaxy!");
        } catch (Exception ex) {
          img = new Image("img/img5.jpg");
        }
        ViewM.setImage(img);
        stage.setScene(mainScene); //maka scene menjadi mainScene
      }
    });

    mBack.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        tfGalaxy.setText("");
        stage.setScene(welcomeScene);
      }
    });

    stage.setScene(welcomeScene);
    stage.sizeToScene();
    stage.show();
  }
}
