import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

/**
 * Class for registration form.
 *
 * The Registration form will ask user to input their: First Name, Last Name,
 * Gender, Username, and Password. After submitting the form, it will check
 * validity of inputted gender, then respond accordingly.
 */
public class RegistrationForm extends Application {
  @Override
  public void start (Stage stage) {
    Group root1 = new Group();
    Scene scene1 = new Scene(root1, 300, 400); /* Creating 1st Scene */

    /* Building input form */
    Label lFName = new Label("First Name");
    TextField tfFName = new TextField();
    Label lLName = new Label("Last Name");
    TextField tfLName = new TextField();
    Label lGender = new Label("Gender (M/F)");
    ComboBox<String> cbGender = new ComboBox<>();
    cbGender.getItems().addAll(
        "Male",
        "Female"
    );
    cbGender.setPromptText("Select Gender...");
    Label lUname = new Label("Username");
    TextField tfUname = new TextField();
    Label lPass = new Label("Password");
    PasswordField pfPass = new PasswordField();
    Button bLogin = new Button("Masuk"); /* Login Button */

    GridPane grid1 = new GridPane();
    grid1.setHgap(10);
    grid1.setVgap(12);
    grid1.add(lFName, 0, 0);
    grid1.add(tfFName, 1, 0);
    grid1.add(lLName, 0, 1);
    grid1.add(tfLName, 1, 1);
    grid1.add(lGender, 0, 2);
    grid1.add(cbGender, 1, 2);
    grid1.add(lUname, 0, 3);
    grid1.add(tfUname, 1, 3);
    grid1.add(lPass, 0, 4);
    grid1.add(pfPass, 1, 4);
    grid1.add(bLogin, 1, 5);
    grid1.setLayoutX(30);
    grid1.setLayoutY(40);
    root1.getChildren().add(grid1);

    Group root2 = new Group();
    Scene scene2 = new Scene(root2, 300, 400); /* Creating 2nd Scene */

    Label masukF = new Label("Selamat Datang, Mbak!");
    masukF.setLayoutX(100);
    masukF.setLayoutY(100);

    Label masukM = new Label("Selamat Datang, Mas!");
    masukM.setLayoutX(100);
    masukM.setLayoutY(100);

    Group root3 = new Group();
    Scene scene3 = new Scene(root3, 300, 400); /* Creating 3rd Scene */

    Label gmasuk = new Label("Gender tidak terdefinisi.");
    gmasuk.setLayoutX(40);
    gmasuk.setLayoutY(40);
    root3.getChildren().add(gmasuk);

    Label gmasuk2 = new Label("Silahkan coba kembali.");
    gmasuk2.setLayoutX(40);
    gmasuk2.setLayoutY(60);
    root3.getChildren().add(gmasuk2);

    /* untuk mencocokkan gender masukan user dengan database-nya */
    bLogin.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        if (cbGender.getValue().equals("Male")) { /* if user is Male */
          root2.getChildren().add(masukM);
          stage.setScene(scene2);
        } else if (cbGender.getValue().equals("Female")) { /* or user is Female */
          root2.getChildren().add(masukF);
          stage.setScene(scene2);
        } else { /* any scenario other than Male/Female */
          stage.setScene(scene3);
        }
      }
    });

    stage.setScene(scene1);
    stage.sizeToScene();
    stage.show();
  }

  public static void main(String[] args) {
    Application.launch(args);
  }
}
