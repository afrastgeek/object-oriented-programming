/**
 * Class for buku.
 */
class Buku extends AlatTulis {
  private String keterangan;
  private String banyakhalaman;
  private int harga;

  /**
  * Constructs the object.
  */
  Buku() {
  }

  /**
  * Sets the keterangan.
  *
  * @param      keterangan  The keterangan
  */
  public void setKeterangan(String keterangan) {
    this.keterangan = keterangan;
  }

  /**
   * Gets the keterangan.
   *
   * @return     The keterangan.
   */
  public String getKeterangan() {
    return this.keterangan;
  }

  /**
  * Sets the banyak halaman.
  *
  * @param      banyakhalaman  The banyakhalaman
  */
  public void setBanyakHalaman(String banyakhalaman) {
    this.banyakhalaman = banyakhalaman;
  }

  /**
   * Gets the banyak halaman.
   *
   * @return     The banyak halaman.
   */
  public String getBanyakHalaman() {
    return this.banyakhalaman;
  }

  /**
  * Sets the harga.
  *
  * @param      harga  The harga
  */
  public void setHarga(int harga) {
    this.harga = harga;
  }

  /**
   * Gets the harga.
   *
   * @return     The harga.
   */
  public int getHarga() {
    return this.harga;
  }
}
