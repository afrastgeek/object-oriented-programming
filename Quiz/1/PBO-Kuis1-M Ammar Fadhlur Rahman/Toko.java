/**
* Class for toko.
*/
class Toko {
  private String kode;
  private String nama;
  private String lokasi;

  /**
   * Constructs the object.
   */
  Toko() {
  }

  /**
  * Sets the kode.
  *
  * @param      kode  The kode
  */
  public void setKode(String kode) {
    this.kode = kode;
  }

  /**
  * Gets the kode.
  *
  * @return     The kode.
  */
  public String getKode() {
    return this.kode;
  }

  /**
   * Sets the nama.
   *
   * @param      nama  The nama
   */
  public void setNama(String nama) {
    this.nama = nama;
  }

  /**
   * Gets the nama.
   *
   * @return     The nama.
   */
  public String getNama() {
    return this.nama;
  }

  /**
   * Sets the lokasi.
   *
   * @param      lokasi  The lokasi
   */
  public void setLokasi(String lokasi) {
    this.lokasi = lokasi;
  }

  /**
   * Gets the lokasi.
   *
   * @return     The lokasi.
   */
  public String getLokasi() {
    return this.lokasi;
  }
}
