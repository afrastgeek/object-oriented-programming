/**
 * Class for pemilik toko.
 */
class PemilikToko {
  private String no_ktp;
  private String nama;
  private String kodetoko;

  /**
   * Constructs the object.
   */
  PemilikToko() {
  }

  /**
   * Sets no ktp.
   *
   * @param      no_ktp  No ktp
   */
  public void setNoKTP(String no_ktp) {
    this.no_ktp = no_ktp;
  }

  /**
   * Gets no ktp.
   *
   * @return     No ktp.
   */
  public String getNoKTP() {
    return this.no_ktp;
  }

  /**
   * Sets the nama.
   *
   * @param      nama  The nama
   */
  public void setNama(String nama) {
    this.nama = nama;
  }

  /**
   * Gets the nama.
   *
   * @return     The nama.
   */
  public String getNama() {
    return this.nama;
  }

  /**
   * Sets the kode toko.
   *
   * @param      kodetoko  The kodetoko
   */
  public void setKodeToko(String kodetoko) {
    this.kodetoko = kodetoko;
  }

  /**
   * Gets the kode toko.
   *
   * @return     The kode toko.
   */
  public String getKodeToko() {
    return this.kodetoko;
  }
}
