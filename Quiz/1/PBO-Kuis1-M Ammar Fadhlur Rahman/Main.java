/*
 * Saya, M Ammar Fadhlur Rahman, tidak melakukan kecurangan seperti yang telah
 * di spesifikasikan pada mata kuliah PBO dalam mengerjakan Kuis 1 PBO, jika
 * saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Amin.
 */

/**
 * Class for main.
 */
class main{
  public static void main(String[] args){

    PemilikToko objek_pemilik;
    Toko objek_toko;
    AlatTulis objek_alattulis;
    Pensil objek_pensil;
    KotakPensil objek_kotakpensil;
    Buku objek_buku;

    objek_pemilik= new PemilikToko();
    objek_pemilik.setNoKTP("7898655232");
    objek_pemilik.setNama("Syukur");
    objek_pemilik.setKodeToko("1");

    objek_toko= new Toko();
    objek_toko.setKode("1");
    objek_toko.setNama("Bahagia");
    objek_toko.setLokasi("Bandung");

    System.out.print("Nama Toko     : ");
    System.out.println(objek_toko.getNama());
    System.out.print("Lokasi      : ");
    System.out.println(objek_toko.getLokasi());
    System.out.print("Nama Pemilik Toko : ");
    System.out.println(objek_pemilik.getNama());
    System.out.print("No KTP      : ");
    System.out.println(objek_pemilik.getNoKTP());


  }
}
