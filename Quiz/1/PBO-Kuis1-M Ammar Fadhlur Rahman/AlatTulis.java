/**
* Class for alat tulis.
*/
class AlatTulis {
  private String kode;
  private String nama;
  private String golongan;
  /**
  * Constructs the object.
  */
  AlatTulis() {
  }

  /**
  * Sets the kode.
  *
  * @param      kode  The kode
  */
  public void setKode(String kode) {
    this.kode = kode;
  }

  /**
  * Gets the kode.
  *
  * @return     The kode.
  */
  public String getKode() {
    return this.kode;
  }

  /**
  * Sets the nama.
  *
  * @param      nama  The nama
  */
  public void setNama(String nama) {
    this.nama = nama;
  }

  /**
  * Gets the nama.
  *
  * @return     The nama.
  */
  public String getNama() {
    return this.nama;
  }

  /**
  * Sets the golongan.
  *
  * @param      golongan  The golongan
  */
  public void setGolongan(String golongan) {
    this.golongan = golongan;
  }

  /**
  * Gets the golongan.
  *
  * @return     The golongan.
  */
  public String getGolongan() {
    return this.golongan;
  }
}
