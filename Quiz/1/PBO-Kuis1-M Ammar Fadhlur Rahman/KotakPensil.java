/**
 * Class for kotak pensil.
 */
class KotakPensil extends AlatTulis {
  private String bahan;
  private String bentuk;

  /**
   * Constructs the object.
   */
  KotakPensil() {
  }

  /**
   * Sets the bahan.
   *
   * @param      bahan  The bahan
   */
  public void setBahan(String bahan) {
    this.bahan = bahan;
  }

  /**
   * Gets the bahan.
   *
   * @return     The bahan.
   */
  public String getBahan() {
    return this.bahan;
  }

  /**
   * Sets the bentuk.
   *
   * @param      bentuk  The bentuk
   */
  public void setBentuk(String bentuk) {
    this.bentuk = bentuk;
  }

  /**
   * Gets the bentuk.
   *
   * @return     The bentuk.
   */
  public String getBentuk() {
    return this.bentuk;
  }
}
