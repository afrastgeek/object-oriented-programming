/**
 * Class for pensil.
 */
class Pensil extends AlatTulis {
  private String jenis;
  private String keterangan;
  private int harga;

  /**
  * Constructs the object.
  */
  Pensil() {
  }

  /**
  * Sets the jenis.
  *
  * @param      jenis  The jenis
  */
  public void setJenis(String jenis) {
    this.jenis = jenis;
  }

  /**
   * Gets the jenis.
   *
   * @return     The jenis.
   */
  public String getJenis() {
    return this.jenis;
  }

  /**
  * Sets the keterangan.
  *
  * @param      keterangan  The keterangan
  */
  public void setKeterangan(String keterangan) {
    this.keterangan = keterangan;
  }

  /**
   * Gets the keterangan.
   *
   * @return     The keterangan.
   */
  public String getKeterangan() {
    return this.keterangan;
  }

  /**
  * Sets the harga.
  *
  * @param      harga  The harga
  */
  public void setHarga(int harga) {
    this.harga = harga;
  }

  /**
   * Gets the harga.
   *
   * @return     The harga.
   */
  public int getHarga() {
    return this.harga;
  }
}
