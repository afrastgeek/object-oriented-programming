/**
 * Class for rekapitulasi pendapatan.
 */
class RekapitulasiPendapatan {
  private String no_ktp;
  private String kodeToko;
  private String bulan;
  private String total;
  private String untung;
  private String modal;

  /**
   * Constructs the object.
   */
  RekapitulasiPendapatan() {

  }

  /**
   * Sets no ktp.
   *
   * @param      no_ktp  No ktp
   */
  public void setNoKTP(String no_ktp) {
    this.no_ktp = no_ktp;
  }

  /**
   * Gets no ktp.
   *
   * @return     No ktp.
   */
  public String getNoKTP() {
    return this.no_ktp;
  }

  /**
   * Sets the kode toko.
   *
   * @param      kodeToko  The kode toko
   */
  public void setKodeToko(String kodeToko) {
    this.kodeToko = kodeToko;
  }

  /**
   * Gets the kode toko.
   *
   * @return     The kode toko.
   */
  public String getKodeToko() {
    return this.kodeToko;
  }

  /**
   * Sets the bulan.
   *
   * @param      bulan  The bulan
   */
  public void setBulan(String bulan) {
    this.bulan = bulan;
  }

  /**
   * Gets the bulan.
   *
   * @return     The bulan.
   */
  public String getBulan() {
    return this.bulan;
  }

  /**
   * Sets the total.
   *
   * @param      total  The total
   */
  public void setTotal(String total) {
    this.total = total;
  }

  /**
   * Gets the total.
   *
   * @return     The total.
   */
  public String getTotal() {
    return this.total;
  }

  /**
   * Sets the untung.
   *
   * @param      untung  The untung
   */
  public void setUntung(String untung) {
    this.untung = untung;
  }

  /**
   * Gets the untung.
   *
   * @return     The untung.
   */
  public String getUntung() {
    return this.untung;
  }

  /**
   * Sets the modal.
   *
   * @param      modal  The modal
   */
  public void setModal(String modal) {
    this.modal = modal;
  }

  /**
   * Gets the modal.
   *
   * @return     The modal.
   */
  public String getModal() {
    return this.modal;
  }
}
