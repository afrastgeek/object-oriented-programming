#include "Fraction.h"

/**
 * @brief      Constructor with no argument.
 */
template <class T> Fraction<T>::Fraction(){
	Fraction<T>::numerator = 0;
	Fraction<T>::denominator = 0;
}

/**
 * @brief      Constructor with arguments to set numerator and denominator.
 */
template <class T> Fraction<T>::Fraction(double n, double d){
	Fraction<T>::numerator = n;
	Fraction<T>::denominator = d;
}

/**
 * @brief      Gets the numerator.
 *
 * @return     The numerator.
 */
template <class T> double Fraction<T>::getNumerator(){
	return Fraction<T>::numerator;
}

/**
 * @brief      Sets the numerator.
 *
 * @param[in]  n     given numerator value
 */
template <class T> void Fraction<T>::setNumerator(double n){
	Fraction<T>::numerator = n;
}

/**
 * @brief      Gets the denominator.
 *
 * @return     The denominator.
 */
template <class T> double Fraction<T>::getDenominator(){
	return Fraction<T>::denominator;
}

/**
 * @brief      Sets the denominator.
 *
 * @param[in]  d     given denominator value
 */
template <class T> void Fraction<T>::setDenominator(double d){
	Fraction<T>::denominator = d;
}

/**
 * @brief      Addition Operator
 *
 * @param      second  The second Fraction
 *
 * @return     Fraction result
 */
template <class T> Fraction<T>::operator+(T &second)
{
    int n1 = getNumerator() * second.getDenominator();
    int n2 = second.getNumerator() * getDenominator();
    int d = getDenominator() * second.getDenominator();
    return Fraction(n1+n2, d);
}

/**
 * @brief      Multiplication Operator
 *
 * @param      second  The second Fraction
 *
 * @return     Fraction result
 */
template <class T> Fraction<T>::operator*(T &second)
{
    int n = getNumerator() * second.getNumerator();
    int d = getDenominator() * second.getDenominator();
    return Fraction(n1, d);
}

/**
 * @brief      Returns a string representation of the object.
 */
template <class T> std::string Fraction<T>::to_string()
{
    std::ostringstream results;
    results << numerator << '/' << denominator;
    return results.str();
}

/**
 * @brief      Desctructor
 */
template <class T> Fraction<T>::~Fraction(){}
