#include <cstdio>
#include <iostream>
using namespace std;
// #include "Mesin.cpp"
#include "Fraction.cpp"
// #include "Tulis.cpp"

/**
 * @brief      Main Program Function
 *
 * @return     Return 0 if the program succeed. Other value if error occurs.
 */
int main(){
	
	double numerator1, numerator2;
	double denominator1, denominator2;

	cout << "Pecahan pertama, input pembilang dan penyebut" << endl;
	cin >> numerator1;
	cin >> denominator1;
	cout << "Pecahan kedua, input pembilang dan penyebut" << endl;
	cin >> numerator2;
	cin >> denominator2;

	Fraction<double> fraction1(numerator1, denominator1);
	Fraction<double> fraction2(numerator2, denominator2);

    Fraction<double> addition_result = fraction1 + fraction2;
    cout << "Addition = " << addition_result.to_string() << endl;

	Fraction<double> multiplication_result = fraction1 * fraction2;
    cout << "Multiplication = " << multiplication_result.to_string() << endl;

		
	return 0;
}