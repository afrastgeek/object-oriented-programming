template <class F>

/**
 * @brief      Class for fraction.
 */
class Fraction{
	private:
		double numerator;
		double denominator;
		
	public:
		Fraction();
		Fraction(double n,double d);
		double getNumerator();
		void setNumerator(double n);
		double getDenominator();
		void setDenominator(double d);
		~Fraction();
};