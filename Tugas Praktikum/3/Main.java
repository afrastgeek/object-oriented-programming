import entity.Asteroid;
import entity.Spacecraft;
import entity.Missile;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.Cursor;
import javafx.scene.input.KeyEvent;
import javafx.animation.KeyFrame;
import javafx.util.Duration;
import javafx.animation.Timeline;
import javafx.animation.Animation;
import javafx.scene.input.KeyCode;

public class Main extends Application {
  private Spacecraft ship;
  private int TIME = 0;

  // mendeklarasikan tabel
  private TableView<Asteroid> table = new TableView<Asteroid>();
  private ObservableList<Asteroid> data =
    FXCollections.observableArrayList();

  int no = 0;
  @Override
  public void start(Stage primaryStage) throws Exception {

    /* welcomeScene Component */

    Label titleLabel_WS = new Label("targetable");
    titleLabel_WS.setFont(new Font(30));
    titleLabel_WS.setLayoutX(135);
    titleLabel_WS.setLayoutY(10);

    Button startButton_WS = new Button("Mulai");
    startButton_WS.setLayoutX(170);
    startButton_WS.setLayoutY(70);

    Button exitButton_WS = new Button("Keluar");
    exitButton_WS.setLayoutX(170);
    exitButton_WS.setLayoutY(120);

    Label authorIDLabel_WS = new Label("1507506");
    authorIDLabel_WS.setLayoutX(10);
    authorIDLabel_WS.setLayoutY(170);

    Label authorLabel_WS = new Label("M Ammar Fadhlur Rahman");
    authorLabel_WS.setLayoutX(250);
    authorLabel_WS.setLayoutY(170);

    Group welcomeRoot = new Group(titleLabel_WS, startButton_WS, exitButton_WS, authorIDLabel_WS, authorLabel_WS);
    Scene welcomeScene = new Scene(welcomeRoot, 400, 200);

    /* tableScene Component */

    Label infoLabel_TS = new Label("Tabel Koordinat Target");
    infoLabel_TS.setLayoutX(10);
    infoLabel_TS.setLayoutY(10);

    // pembuatan kolom pada tabel
    // kolom nomor
    TableColumn<Asteroid, String> NoCol = new TableColumn<Asteroid, String>("No");
    NoCol.setMaxWidth(50);
    // mengintegrasikan kolom pada tabel dengan data
    NoCol.setCellValueFactory(
      new PropertyValueFactory<Asteroid, String>("No")
    );

    // kolom kode barang
    TableColumn<Asteroid, String> xCol = new TableColumn<Asteroid, String>("X");
    xCol.setMaxWidth(50);
    xCol.setCellValueFactory(
      new PropertyValueFactory<Asteroid, String>("X")
    );

    // kolom jenis barang
    TableColumn<Asteroid, String> yCol = new TableColumn<Asteroid, String>("Y");
    yCol.setMaxWidth(50);
    yCol.setCellValueFactory(
      new PropertyValueFactory<Asteroid, String>("Y")
    );

    TableColumn<Asteroid, String> bobotCol = new TableColumn<Asteroid, String>("Bobot");
    bobotCol.setMaxWidth(80);
    bobotCol.setCellValueFactory(
      new PropertyValueFactory<Asteroid, String>("Bobot")
    );

    // add kolom ke tabel
    table.getColumns().addAll(NoCol, xCol, yCol, bobotCol);
    table.setItems(data);
    table.setLayoutX(45);
    table.setLayoutY(40);
    table.setMaxWidth(235);
    table.setMaxHeight(300);

    TextField xTextField_TS = new TextField();
    xTextField_TS.setPromptText("X");
    xTextField_TS.setMaxWidth(xCol.getMaxWidth());

    TextField yTextField_TS = new TextField();
    yTextField_TS.setPromptText("Y");
    yTextField_TS.setMaxWidth(yCol.getMaxWidth());

    TextField bobotTextField_TS = new TextField();
    bobotTextField_TS.setPromptText("Bobot");
    bobotTextField_TS.setMaxWidth(bobotCol.getMaxWidth());

    Button tambahButton_TS = new Button("Tambah");

    tambahButton_TS.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        no++;
        // add data ke list
        data.add(new Asteroid(
                   "" + no,
                   xTextField_TS.getText(),
                   yTextField_TS.getText().toString(),
                   bobotTextField_TS.getText()
                 ));
        xTextField_TS.setText("");
        yTextField_TS.setText("");
        bobotTextField_TS.setText("");
      }
    });

    HBox addHBox_TS = new HBox(xTextField_TS, yTextField_TS, bobotTextField_TS, tambahButton_TS);
    addHBox_TS.setSpacing(5);
    addHBox_TS.setLayoutX(38);
    addHBox_TS.setLayoutY(350);

    TextField noTextField_TS = new TextField();
    noTextField_TS.setPromptText("No");
    noTextField_TS.setMaxWidth(NoCol.getMaxWidth());

    Button hapusButton_TS = new Button("Hapus");

    hapusButton_TS.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        int selection = Integer.parseInt(noTextField_TS.getText());
        try {
          data.remove(selection - 1);

          // ListIterator<Asteroid> dataMap = data.listIterator(selection);
          // while (dataMap.hasNext()) {
          //   dataMap.No = selection;
          // }

          table.setItems(data);
        } catch (Exception err) {}
        noTextField_TS.setText("");
      }
    });

    HBox delHBox_TS = new HBox(noTextField_TS, hapusButton_TS);
    delHBox_TS.setSpacing(5);
    delHBox_TS.setLayoutX(178);
    delHBox_TS.setLayoutY(380);

    Button mulaiButton_TS = new Button("Mulai Game");
    mulaiButton_TS.setLayoutX(120);
    mulaiButton_TS.setLayoutY(428);

    final Group tableRoot = new Group();
    tableRoot.getChildren().addAll(infoLabel_TS, table, addHBox_TS, delHBox_TS , mulaiButton_TS);

    Scene tableScene = new Scene(tableRoot, 340, 500);

    startButton_WS.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        primaryStage.setScene(tableScene);
        primaryStage.centerOnScreen();
      }
    });

    // membuat ship jagoan
    ship = new Spacecraft();
    ship.setWidth(50);
    ship.setPosition(250 - ship.getRadius(), 475 - (ship.getRadius() * 2));
    ship.setVelocity(1);

    Group mainRoot = new Group();
    Scene mainScene = new Scene(mainRoot, 500, 500);

    mainScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
      boolean isSpacePressed = false;
      @Override
      public void handle(KeyEvent event) {
        KeyCode key = event.getCode();
        switch (key) {
        case SPACE:
          ship.shoot(mainRoot);
          isSpacePressed = true;
          break;

        case RIGHT:
          ship.setPosition(ship.getX() + 5, ship.getY());
          if (isSpacePressed == true) {
            ship.shoot(mainRoot);
          }
          break;

        case LEFT:
          ship.setPosition(ship.getX() - 5, ship.getY());
          if (isSpacePressed == true) {
            ship.shoot(mainRoot);
          }
          break;

        }
      }
    });
    ship.addToGroup(mainRoot);

    KeyFrame oneFrame = new KeyFrame(Duration.millis(1000 / 60), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        // untuk menggerakan peluru
        ship.updateMissile(mainRoot);
        TIME++;
      }
    });

    Timeline tl = new Timeline();
    tl.setCycleCount(Animation.INDEFINITE);
    tl.getKeyFrames().add(oneFrame);

    mulaiButton_TS.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        primaryStage.setScene(mainScene);
        primaryStage.centerOnScreen();
        tl.play();
      }
    });

    primaryStage.setTitle("Targetable");
    primaryStage.setScene(welcomeScene);
    primaryStage.centerOnScreen();
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
