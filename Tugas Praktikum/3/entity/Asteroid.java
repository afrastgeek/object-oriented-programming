package entity;

import javafx.beans.property.SimpleStringProperty;

public class Asteroid {
  //jenis datanya
  public final SimpleStringProperty No;
  public final SimpleStringProperty X;
  public final SimpleStringProperty Y;
  public final SimpleStringProperty Bobot;

  public Asteroid(String Noarg, String Xarg, String Yarg, String Bobotarg) {
    this.No = new SimpleStringProperty(Noarg);
    this.X = new SimpleStringProperty(Xarg);
    this.Y = new SimpleStringProperty(Yarg);
    this.Bobot = new SimpleStringProperty(Bobotarg);
  }

  // fungsi untuk mendapatkan no
  public String getNo() {
    return No.get();
  }

  public void setNo(String Noarg) {
    No.set(Noarg);
  }

  // fungsi untuk mendapatkan x
  public String getX() {
    return X.get();
  }

  public void setX(String Xarg) {
    X.set(Xarg);
  }

  // fungsi untuk mendapatkan y
  public String getY() {
    return Y.get();
  }

  public void setY(String Yarg) {
    Y.set(Yarg);
  }

  // fungsi untuk mendapatkan bobot
  public String getBobot() {
    return Bobot.get();
  }

  public void setBobot(String Bobotarg) {
    Bobot.set(Bobotarg);
  }
}
