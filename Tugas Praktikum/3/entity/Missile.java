/*
 * class ini digunakan untuk membuat peluru dengan bentuk persegi
 */
package entity;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Missile extends Rectangle {

  Missile(double x, double y) {
    /*
     * ini merupakan constructor
     * nilai dari masukan/parameternya akan dijadikan koordinat awal dari peluru
     */
    this.setX(x - 5);
    this.setY(y);
    setWidth(10);
    setHeight(10);
    setStroke(Color.GREEN);
    setFill(Color.RED);
  }
}
