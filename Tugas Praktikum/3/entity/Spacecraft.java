/*
 * Class ini digunakan untuk membuat bola yang menembakan missile
 * terdapat juga pengaturan pada missile
 */
package entity;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Spacecraft extends Circle {
  Rectangle rect;
  float velocity;
  double x, y;

  // array untuk menampung missile
  ArrayList<Missile> arrMissile = new ArrayList<Missile>();
  ArrayList<Missile> arrMissileKiri = new ArrayList<Missile>();
  ArrayList<Missile> arrMissileKanan = new ArrayList<Missile>();

  public Spacecraft() {
    // constructor
    rect = new Rectangle();
    rect.setStroke(Color.GREEN);
    rect.setFill(null);
    setFill(Color.RED);
  }

  public void setWidth(double width) {
    // prosedur yang digunakan untuk menentukan ukuran bola
    setRadius(width / 2);
    rect.setWidth(width);
    rect.setHeight(width);
  }

  public void setPosition(double x, double y) {
    // prosedur yang digunakan untuk mengatur posisi bola
    this.x = x;
    this.y = y;
    setLayoutX(x + getRadius());
    setLayoutY(y + getRadius());
    rect.setX(x);
    rect.setY(y);
  }

  public double getX() {
    // fungsi untuk mendapatkan nilai koordinat x dari bola jagoan
    return x;
  }

  public double getY() {
    // fungsi untuk mendapatkan nilai koordinat y dari bola jagoan
    return y;
  }

  public Rectangle getRectangle() {
    // fungsi untuk mendapatkan persegi dari bola jagoan
    return rect;
  }

  public float getVelocity() {
    // fungsi untuk mendapatkan nilai percepatan bola jagoan
    return velocity;
  }

  public void setVelocity(float velocity) {
    // prosedur untuk menentukan nilai percepatan bola jagoan
    this.velocity = velocity;
  }

  public void addToGroup(Group root) {
    // add bola jagoan ke scene
    root.getChildren().add(this);
    root.getChildren().add(rect);
  }

  public void shoot(Group root) {
    // prosedur untuk membuat missile tengah
    Missile missile = new Missile(x + getRadius(), y);
    arrMissile.add(missile);
    root.getChildren().add(missile);
  }

  public void shootKiri(Group root) {
    // prosedur untuk membuat missile kiri
    Missile missile = new Missile(x + getRadius(), y);
    arrMissileKiri.add(missile);
    root.getChildren().add(missile);
  }

  public void shootKanan(Group root) {
    // prosedur untuk membuat missile kanan
    Missile missile = new Missile(x + getRadius(), y);
    arrMissileKanan.add(missile);
    root.getChildren().add(missile);
  }

  public void updateMissile(Group root) {
    // prosedur untuk menembakan missile tengah
    // array untuk menampung missile yang akan dihapus
    ArrayList<Missile> delMissile = new ArrayList<Missile> ();
    for (Missile missile : arrMissile) {
      //untuk menggerakan missile ke atas
      missile.setY(missile.getY() - 5);
      // jika missile sudah melewati batas
      // maka dimasukkan ke array untuk hapus
      if (missile.getY() < 0) {
        delMissile.add(missile);
      }
    }
    // menghapus missile
    for (Missile missile : delMissile) {
      arrMissile.remove(missile);
      root.getChildren().remove(missile);
    }
  }

  public void updateMissileKiri(Group root) {
    // prosedur untuk menembakan missile kiri
    // array untuk menampung missile yang akan dihapus
    ArrayList<Missile> delMissile = new ArrayList<Missile> ();
    for (Missile missileKiri : arrMissileKiri) {
      //untuk menggerakan missile ke atas
      missileKiri.setY(missileKiri.getY() - 5);
      missileKiri.setX(missileKiri.getX() - 3);
      // jika missile sudah melewati batas
      // maka dimasukkan ke array untuk hapus
      if (missileKiri.getY() < 0) {
        delMissile.add(missileKiri);
      }
    }
    // menghapus missile
    for (Missile missile : delMissile) {
      arrMissileKiri.remove(missile);
      root.getChildren().remove(missile);
    }
  }

  public void updateMissileKanan(Group root) {
    // prosedur untuk menembakan missile Kanan
    // array untuk menampung missile yang akan dihapus
    ArrayList<Missile> delMissile = new ArrayList<Missile> ();
    for (Missile missileKanan : arrMissileKanan) {
      //untuk menggerakan missile ke atas
      missileKanan.setY(missileKanan.getY() - 5);
      missileKanan.setX(missileKanan.getX() + 4);
      // jika missile sudah melewati batas
      // maka dimasukkan ke array untuk hapus
      if (missileKanan.getY() < 0) {
        delMissile.add(missileKanan);
      }
    }
    // menghapus missile
    for (Missile missile : delMissile) {
      arrMissileKanan.remove(missile);
      root.getChildren().remove(missile);
    }
  }
}
