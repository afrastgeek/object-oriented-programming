/*
 * Saya, M Ammar Fadhlur Rahman, tidak melakukan kecurangan seperti yang telah
 * di spesifikasikan pada mata kuliah PBO dalam mengerjakan Tugas Praktikum 1,
 * jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Amin.
 */
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PhoneFinder extends Application {
	@Override public void start(Stage stage) {
		/*
		 * Login Scene
		 */
		Group loginGroup = new Group();
		Scene loginScene = new Scene(loginGroup);

		Text textTitle1 = new Text("PBO CELL");
		textTitle1.setFont(Font.font(30));
		textTitle1.setLayoutX(100);
		textTitle1.setLayoutY(30);
		loginGroup.getChildren().add(textTitle1);

		Text textSubtitle1 = new Text("Cellular phones & Accesories");
		textSubtitle1.setFont(Font.font(18));
		textSubtitle1.setLayoutX(60);
		textSubtitle1.setLayoutY(50);
		loginGroup.getChildren().add(textSubtitle1);

		Label lUsername = new Label("Username :");
		lUsername.setFont(Font.font(14));
		lUsername.setLayoutX(60);
		lUsername.setLayoutY(80);
		loginGroup.getChildren().add(lUsername);

		TextField tfUsername = new TextField();
		tfUsername.setLayoutX(145);
		tfUsername.setLayoutY(80);
		loginGroup.getChildren().add(tfUsername);

		Label lPassword = new Label("Password  :");
		lPassword.setFont(Font.font(14));
		lPassword.setLayoutX(60);
		lPassword.setLayoutY(110);
		loginGroup.getChildren().add(lPassword);

		PasswordField pfPassword = new PasswordField();
		pfPassword.setLayoutX(145);
		pfPassword.setLayoutY(110);
		loginGroup.getChildren().add(pfPassword);

		Button bLogin = new Button("Login");
		bLogin.setLayoutX(245);
		bLogin.setLayoutY(150);
		loginGroup.getChildren().add(bLogin);

		Label lNama1 = new Label("1507506_M Ammar Fadhlur Rahman");
		lNama1.setFont(Font.font(10));
		lNama1.setLayoutX(180);
		lNama1.setLayoutY(190);
		loginGroup.getChildren().add(lNama1);

		/*
		 * Selection Scene
		 */
		Group selectionGroup = new Group();
		Scene selectionScene = new Scene(selectionGroup, 530, 350);

		Text textTitle2 = new Text("PBO CELL");
		textTitle2.setFont(Font.font(30));
		textTitle2.setLayoutX(195);
		textTitle2.setLayoutY(30);
		selectionGroup.getChildren().add(textTitle2);

		Text scenetitle4 = new Text("Cellular phones & Accesories");
		scenetitle4.setFont(Font.font(18));
		scenetitle4.setLayoutX(150);
		scenetitle4.setLayoutY(50);
		selectionGroup.getChildren().add(scenetitle4);

		Text textWelcome1 = new Text("Selamat datang ");
		textWelcome1.setFont(Font.font(12));
		textWelcome1.setLayoutX(210);
		textWelcome1.setLayoutY(90);
		selectionGroup.getChildren().add(textWelcome1);

		Text textWelcome2 = new Text("Silahkan pilih vendor dan range harga untuk mencari dan");
		textWelcome2.setFont(Font.font(12));
		textWelcome2.setLayoutX(115);
		textWelcome2.setLayoutY(110);
		selectionGroup.getChildren().add(textWelcome2);

		Text textWelcome3 = new Text("melihat spesifikasi produk secara lengkap");
		textWelcome3.setFont(Font.font(12));
		textWelcome3.setLayoutX(155);
		textWelcome3.setLayoutY(130);
		selectionGroup.getChildren().add(textWelcome3);

		Label lVendor = new Label("Vendor  : ");
		lVendor.setFont(Font.font(16));
		lVendor.setLayoutX(170);
		lVendor.setLayoutY(170);
		selectionGroup.getChildren().add(lVendor);
		ComboBox<String> cbVendor = new ComboBox<>();
		cbVendor.getItems().addAll(
		  "Samsung",
		  "ASUS"
		);
		cbVendor.setValue("Samsung");
		cbVendor.setLayoutX(270);
		cbVendor.setLayoutY(170);
		selectionGroup.getChildren().add(cbVendor);

		Label lHarga = new Label("Harga   : ");
		lHarga.setFont(Font.font(16));
		lHarga.setLayoutX(170);
		lHarga.setLayoutY(200);
		selectionGroup.getChildren().add(lHarga);
		ComboBox<String> cbHarga = new ComboBox<>();
		cbHarga.getItems().addAll(
		  "<2jt",
		  "2-3,5jtan",
		  "3,5-5jtan",
		  ">5jt"
		);
		cbHarga.setValue("<2jt");
		cbHarga.setLayoutX(270);
		cbHarga.setLayoutY(200);
		selectionGroup.getChildren().add(cbHarga);

		Button bCari = new Button("Cari Produk");
		bCari.setLayoutX(235);
		bCari.setLayoutY(250);
		selectionGroup.getChildren().add(bCari);

		Label lNama2 = new Label("1507506_M Ammar Fadhlur Rahman");
		lNama2.setFont(Font.font(10));
		lNama2.setLayoutX(180);
		lNama2.setLayoutY(335);
		selectionGroup.getChildren().add(lNama2);

		/*
		 * Detail Scene
		 */
		Group detailGroup = new Group();
		Scene detailScene = new Scene(detailGroup, 530, 350);

		Text textTitle3 = new Text("PBO CELL");
		textTitle3.setFont(Font.font(30));
		textTitle3.setLayoutX(195);
		textTitle3.setLayoutY(30);
		detailGroup.getChildren().add(textTitle3);
		Text textSubtitle3 = new Text("Cellular phones & Accesories");
		textSubtitle3.setFont(Font.font(18));
		textSubtitle3.setLayoutX(150);
		textSubtitle3.setLayoutY(50);
		detailGroup.getChildren().add(textSubtitle3);

		Text namaProduk = new Text();
		namaProduk.setFont(Font.font(16));
		namaProduk.setLayoutX(200);
		namaProduk.setLayoutY(100);
		detailGroup.getChildren().add(namaProduk);

		Image img1 = new Image("img/1.jpg");
		Image img2 = new Image("img/2.jpg");
		Image img3 = new Image("img/3.jpg");
		Image img4 = new Image("img/4.jpg");
		Image img5 = new Image("img/5.jpg");
		Image img6 = new Image("img/6.jpg");
		Image img7 = new Image("img/7.jpg");
		Image img8 = new Image("img/8.jpg");
		ImageView ViewM = new ImageView();
		ViewM.setLayoutX(55);
		ViewM.setLayoutY(125);
		ViewM.setFitHeight(135);
		ViewM.setFitWidth(150);
		detailGroup.getChildren().add(ViewM);


		Text textSpecification = new Text("Spesifikasi Produk:");
		textSpecification.setFont(Font.font(14));
		textSpecification.setLayoutX(325);
		textSpecification.setLayoutY(135);
		detailGroup.getChildren().add(textSpecification);
		Text textScreen = new Text("Layar :");
		textScreen.setFont(Font.font(13));
		textScreen.setLayoutX(260);
		textScreen.setLayoutY(160);
		detailGroup.getChildren().add(textScreen);
		Text textOS = new Text("OS :");
		textOS.setFont(Font.font(13));
		textOS.setLayoutX(260);
		textOS.setLayoutY(180);
		detailGroup.getChildren().add(textOS);
		Text textMemory = new Text("RAM :");
		textMemory.setFont(Font.font(13));
		textMemory.setLayoutX(260);
		textMemory.setLayoutY(200);
		detailGroup.getChildren().add(textMemory);
		Text textStorage = new Text("Memori :");
		textStorage.setFont(Font.font(13));
		textStorage.setLayoutX(260);
		textStorage.setLayoutY(220);
		detailGroup.getChildren().add(textStorage);
		Text textBattery = new Text("Baterai :");
		textBattery.setFont(Font.font(13));
		textBattery.setLayoutX(260);
		textBattery.setLayoutY(240);
		detailGroup.getChildren().add(textBattery);
		Text textCamera = new Text("Kamera :");
		textCamera.setFont(Font.font(13));
		textCamera.setLayoutX(260);
		textCamera.setLayoutY(260);
		detailGroup.getChildren().add(textCamera);

		Button bBack = new Button("Kembali");
		bBack.setLayoutX(235);
		bBack.setLayoutY(285);
		detailGroup.getChildren().add(bBack);

		Label lNama3 = new Label("1507506_M Ammar Fadhlur Rahman");
		lNama3.setFont(Font.font(10));
		lNama3.setLayoutX(180);
		lNama3.setLayoutY(335);
		detailGroup.getChildren().add(lNama3);

		bLogin.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				String username = tfUsername.getText();
				String password = pfPassword.getText();
				if (password.equals("pboceria")) {
					textWelcome1.setText("Selamat datang " + username);
					stage.setScene(selectionScene);
				} else if (password.equals("") &&  username.equals("")) {
					JOptionPane.showMessageDialog(null, "Field shouldn't be empty!");
				} else {
					JOptionPane.showMessageDialog(null, "Wrong Password!");
					tfUsername.setText("");
					pfPassword.setText("");
				}
			}
		});

		bCari.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (cbVendor.getValue().equals("Samsung")) {
					if (cbHarga.getValue().equals("<2jt")) {
						ViewM.setImage(img1);
						namaProduk.setText("Samsung Galaxy J1");
						textScreen.setText("Layar : 4,3 inch");
						textOS.setText("OS : Android OS, v4.4.4 (KitKat)");
						textMemory.setText("RAM : 512 MB");
						textStorage.setText("Memori : 4 GB, MicroSD up to 128 GB");
						textBattery.setText("Baterai : Li-Ion 1850 mAh");
						textCamera.setText("Depan : 2 MP, Belakang : 5 MP");
					} else if (cbHarga.getValue().equals("2-3,5jtan")) {
						ViewM.setImage(img2);
						namaProduk.setText("Samsung Galaxy A3");
						textScreen.setText("Layar : 4,7 inch");
						textOS.setText("OS : Android OS, v5.1.1 (Lollipop)");
						textMemory.setText("RAM : 1.5 GB");
						textStorage.setText("Memori : 16 GB, microSD, up to 256 GB");
						textBattery.setText("Baterai : Li-Ion 2300 mAh");
						textCamera.setText("Depan : 5 MP, Belakang : 13 MP");
					} else if (cbHarga.getValue().equals("3,5-5jtan")) {
						ViewM.setImage(img3);
						namaProduk.setText("Samsung Galaxy A5");
						textScreen.setText("Layar : 5 inch");
						textOS.setText("OS : Android OS, v4.4.4 (KitKat)");
						textMemory.setText("RAM : 2 GB");
						textStorage.setText("Memori : 	16 GB, microSD, up to 64 GB");
						textBattery.setText("Baterai : Li-Ion 2300 mAh");
						textCamera.setText("Depan : 5 MP, Belakang : 13 MP");
					} else if (cbHarga.getValue().equals(">5jt")) {
						ViewM.setImage(img4);
						namaProduk.setText("Samsung Galaxy A8");
						textScreen.setText("Layar : 5,7 inch");
						textOS.setText("OS : Android OS, v5.1 (Lollipop)");
						textMemory.setText("RAM : 2 GB");
						textStorage.setText("Memori : 	32 GB, microSD, up to 128 GB");
						textBattery.setText("Baterai : Li-Ion 3050 mAh");
						textCamera.setText("Depan : 5 MP, Belakang : 16 MP");
					}
					stage.setScene(detailScene);
				} else if (cbVendor.getValue().equals("ASUS")) {
					if (cbHarga.getValue().equals("<2jt")) {
						ViewM.setImage(img5);
						namaProduk.setText("Asus Zenfone 4");
						textScreen.setText("Layar : 4,3 inch");
						textOS.setText("OS : Android OS, v4.3 (Jelly Bean)");
						textMemory.setText("RAM : 1 GB");
						textStorage.setText("Memori : 4/8 GB, MicroSD up to 64 GB");
						textBattery.setText("Baterai : 1200 mAh Lithium");
						textCamera.setText("Depan : VGA , Belakang : 5 MP");
					} else if (cbHarga.getValue().equals("2-3,5jtan")) {
						ViewM.setImage(img6);
						namaProduk.setText("Asus Zenfone 2 ZE550ML");
						textScreen.setText("Layar : 5,5 inch");
						textOS.setText("OS : Android 5.0.1 (Lollipop)");
						textMemory.setText("RAM : 2 GB");
						textStorage.setText("Memori : 16 GB, MicroSD up to 64 GB");
						textBattery.setText("Baterai : 3000 mAh");
						textCamera.setText("Depan : 5 MP, Belakang : 13 MP");
					} else if (cbHarga.getValue().equals("3,5-5jtan")) {
						ViewM.setImage(img7);
						namaProduk.setText("Asus Zenfone 3");
						textScreen.setText("Layar : 5,5 inch");
						textOS.setText("OS : Android v6.0.1 (Marshmallow)");
						textMemory.setText("RAM : 3/4 GB");
						textStorage.setText("Memori : 32/64 GB, MicroSD up to 128 GB");
						textBattery.setText("Baterai : 3000 mAh");
						textCamera.setText("Depan : 8 MP, Belakang : 16 MP");
					} else if (cbHarga.getValue().equals(">5jt")) {
						ViewM.setImage(img8);
						namaProduk.setText("Asus ZenFone 2 Deluxe");
						textScreen.setText("Layar : 5,5 inch");
						textOS.setText("OS : Android OS, v5.0 (Lollipop)");
						textMemory.setText("RAM : 4 GB");
						textStorage.setText("Memori : 64 GB, MicroSD up to 128 GB");
						textBattery.setText("Baterai : 3000 mAh");
						textCamera.setText("Depan : 5 MP, Belakang : 13 MP");
					}
					stage.setScene(detailScene);
				}
			}
		});

		bBack.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				stage.setScene(selectionScene);
			}
		});

		stage.setScene(loginScene);
		stage.sizeToScene();
		stage.show();
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
