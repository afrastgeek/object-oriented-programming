package entity;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * Class for ball.
 */
public class Ball extends Circle {
  float velocity;
  double x, y;

  /**
   * Constructs the object.
   */
  public Ball() {
    setFill(Color.RED);
  }

  /**
   * Sets the width.
   *
   * @param      width  The width
   */
  public void setWidth(double width) {
    // prosedur yang digunakan untuk menentukan ukuran bola
    setRadius(width / 2);
  }

  /**
   * Sets the position.
   *
   * @param      x     { parameter_description }
   * @param      y     { parameter_description }
   */
  public void setPosition(double x, double y) {
    // prosedur yang digunakan untuk mengatur posisi bola
    this.x = x;
    this.y = y;
    setLayoutX(x + getRadius());
    setLayoutY(y + getRadius());
  }

  /**
   * Gets the x.
   *
   * @return     The x.
   */
  public double getX() {
    // fungsi untuk mendapatkan nilai koordinat x dari bola jagoan
    return x;
  }

  /**
   * Gets the y.
   *
   * @return     The y.
   */
  public double getY() {
    // fungsi untuk mendapatkan nilai koordinat y dari bola jagoan
    return y;
  }

  /**
   * Gets the velocity.
   *
   * @return     The velocity.
   */
  public float getVelocity() {
    return velocity;
  }

  /**
   * Sets the velocity.
   *
   * @param      velocity  The velocity
   */
  public void setVelocity(float velocity) {
    this.velocity = velocity;
  }

  /**
   * Adds to group.
   *
   * @param      canvas  The canvas
   */
  public void addToGroup(Pane canvas) {
    canvas.getChildren().add(this);
  }
}
