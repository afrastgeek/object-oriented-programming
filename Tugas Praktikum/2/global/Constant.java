package global;

import javafx.util.Duration;

/**
 * Class for constant.
 */
public class Constant {
  public static final String TITLE = "Bounce";
  public static final String AUTHOR_NAME = "M Ammar Fadhlur Rahman";
  public static final String AUTHOR_NIM = "1507506";
  public static final String START_TEXT = "Mulai";
  public static final String ADD_BALL_TEXT = "Tambah";
  public static final Duration FPS = Duration.millis(1000 / 60);
}
