/*
 * Saya, M Ammar Fadhlur Rahman, tidak melakukan kecurangan seperti yang telah
 * di spesifikasikan pada mata kuliah PBO dalam mengerjakan Tugas Praktikum 1,
 * jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Amin.
 */
import javafx.application.Application;
import javafx.scene.layout.HBox;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.control.Button;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.geometry.Pos;
import javafx.geometry.HPos;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Screen;
import javafx.geometry.Rectangle2D;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import entity.Ball;
import global.Constant;

public class Bounce extends Application {
  @Override public void start(Stage stage) {
    Text title = new Text(Constant.TITLE);
    Text nim = new Text(Constant.AUTHOR_NIM);
    Text name = new Text(Constant.AUTHOR_NAME);

    Button startButton = new Button(Constant.START_TEXT);

    GridPane infobar = new GridPane();
    infobar.setPrefWidth(300);
    infobar.add(nim, 0, 0);
    infobar.add(name, 1, 0);
    GridPane.setHalignment(name, HPos.RIGHT);

    BorderPane borderPane = new BorderPane();
    borderPane.setPrefSize(300, 100);
    borderPane.setAlignment(title, Pos.CENTER);
    borderPane.setTop(title);
    borderPane.setCenter(startButton);
    borderPane.setBottom(infobar);

    Scene welcomeScene = new Scene(borderPane);

    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

    Button addBallButton = new Button(Constant.ADD_BALL_TEXT);
    addBallButton.setLayoutX(primaryScreenBounds.getWidth() - 70);
    addBallButton.setLayoutY(10);

    Pane canvas = new Pane();
    canvas.getChildren().add(addBallButton);
    Scene mainScene = new Scene(canvas, primaryScreenBounds.getWidth(), primaryScreenBounds.getHeight());

    KeyFrame frame = new KeyFrame(Constant.FPS, new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        // Animation goes here
      }
    });

    startButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        stage.setScene(mainScene);
        stage.setFullScreen(true);

        Timeline timeline = new Timeline();
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.getKeyFrames().add(frame);
        timeline.play();
      }
    });

    addBallButton.setOnAction(new EventHandler<ActionEvent() {
      @Override
      public void handle(ActionEvent e) {
        // Add Ball mechanism here
      }
    });

    stage.setScene(welcomeScene);
    stage.sizeToScene();
    stage.show();
  }

  public static void main() {
    Application.launch();
  }
}
