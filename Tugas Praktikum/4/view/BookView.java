package view;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import controller.BookPresenter;
import model.Book;

public class BookView {

  public BookView() {}

  private TableView<Book> tableView = new TableView<Book>();
  private final ObservableList<Book> data = FXCollections.observableArrayList();
  private HBox textFieldHBox = new HBox();
  private HBox buttonHBox = new HBox();

  public void show(Stage stage) {
    stage.setTitle("CRUD UI");
    stage.setWidth(400);
    stage.setHeight(600);

    BookPresenter bookPresenter = new BookPresenter();
    Scene scene = new Scene(new Group());

    final Label formTitle = new Label("Daftar Buku Babe");

    TableColumn<Book, String> titleColumn = new TableColumn<Book, String>("Judul buku");
    // titleColumn.setMaxWidth(250);
    titleColumn.setCellValueFactory(
      new PropertyValueFactory<Book, String>("Title")
    );

    TableColumn<Book, String> authorColumn = new TableColumn<Book, String>("Penulis");
    // authorColumn.setMaxWidth(150);
    authorColumn.setCellValueFactory(
      new PropertyValueFactory<Book, String>("Author")
    );

    tableView.getColumns().addAll(titleColumn, authorColumn);
    tableView.setItems(data);

    final VBox rootVBox = new VBox();
    rootVBox.setSpacing(10);
    rootVBox.getChildren().addAll(formTitle, tableView, buttonHBox, textFieldHBox);
    rootVBox.setPadding(new Insets(10, 0, 0, 10));
    rootVBox.setPrefWidth(375);

    int jumlah = 0, i = 0;
    String[][] daftar = new String[25][5];

    try {
      bookPresenter.prosesBku();
      jumlah = bookPresenter.getJml();
      daftar = bookPresenter.getHasil();
    } catch (Exception e) {
      System.out.println(bookPresenter.getError());
    }

    for (i = 0; i < jumlah; i++) {
      data.add(new Book(daftar[i][0], daftar[i][1]));
    }

    tableView.setItems(data);

    final TextField titleTextField = new TextField();
    titleTextField.setPromptText("Judul Buku");
    titleTextField.setMaxWidth(130);

    final TextField authorTextField  = new TextField();
    authorTextField .setPromptText("Nama Penulis");
    authorTextField .setMaxWidth(130);

    final Button deleteButton = new Button("Hapus");
    deleteButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        Book selectedItem = tableView.getSelectionModel().getSelectedItem();
        String judul = selectedItem.getTitle();
        String penulis = selectedItem.getAuthor();
        bookPresenter.hapusbuku(judul);
        tableView.getItems().remove(selectedItem);
      }
    });

    final Button addButton = new Button("Tambah");
    addButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        bookPresenter.tambahBuku(titleTextField.getText(), authorTextField  .getText());
        data.add(new Book(titleTextField.getText(), authorTextField .getText()));

        titleTextField.setText("");
        authorTextField .setText("");
      }
    });

    final Button editButton = new Button("Ganti");
    editButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent e) {
        Book selectedItem = tableView.getSelectionModel().getSelectedItem();
        String judul = selectedItem.getTitle();
        String penulis = selectedItem.getAuthor();
        tableView.getItems().remove(selectedItem);
        titleTextField.setText("" + judul);
        authorTextField .setText("" + penulis);
      }
    });

    final Button saveButton = new Button("Simpan");
    saveButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent stage) {
        String judul = titleTextField.getText();
        String penulis = authorTextField.getText();
        bookPresenter.editbuku(judul, penulis);

        int jumlah = 0, i = 0;
        String[][] daftar = new String[25][5];

        try {
          bookPresenter.prosesBku();
          jumlah = bookPresenter.getJml();
          daftar = bookPresenter.getHasil();
        } catch (Exception e) {
          System.out.println(bookPresenter.getError());
        }

        for (i = 0; i < jumlah; i++) {
          data.add(new Book(daftar[i][0], daftar[i][1]));
        }

        tableView.setItems(data);

        titleTextField.setText("");
        authorTextField .setText("");
      }
    });

    buttonHBox.getChildren().addAll(addButton, deleteButton , editButton);
    buttonHBox.setAlignment(Pos.CENTER);
    buttonHBox.setSpacing(10);

    textFieldHBox.getChildren().addAll(titleTextField, authorTextField , saveButton);
    textFieldHBox.setAlignment(Pos.CENTER);
    textFieldHBox.setSpacing(10);

    ((Group) scene.getRoot()).getChildren().addAll(rootVBox);

    stage.setScene(scene);
    stage.show();
  }
}
