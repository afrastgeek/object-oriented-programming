package model;

import javafx.beans.property.SimpleStringProperty;

public class Book {
  private final SimpleStringProperty title;
  private final SimpleStringProperty author;

  public Book(String title, String author) {
    this.title = new SimpleStringProperty(title);
    this.author = new SimpleStringProperty(author);
  }

  public String getTitle() {
    return title.get();
  }

  public void setTitle(String title) {
    this.title.set(title);
  }

  public String getAuthor() {
    return author.get();
  }

  public void setAuthor(String author) {
    this.author.set(author);
  }
}
