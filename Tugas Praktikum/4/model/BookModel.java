package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class BookModel extends Database {

  private String kode_buku;
  private String judul_buku;
  private String penulis_buku;

  public BookModel() throws Exception, SQLException {
    //konstruktor
    super();
  }

  //menampilkan semua data yang ada di database
  public void getDaftar_Buku() {

    try {
      String query = "SELECT * FROM `daftar_buku`";
      query(query);
    } catch (Exception e) {
      System.out.println(e.toString());
    }
  }

  //menghapus data yang ada di database sesuai dengan judul buku
  public void hapusBuku(String judul) {
    try {
      String query = "DELETE FROM `daftar_buku` WHERE judul_buku = '" + judul + "'";
      update(query);
    } catch (Exception e) {
      System.out.println(e.toString());
    }
  }

  //menambah data database
  public void insertBuku(String judul, String penulis) {
    try {
      String query = "INSERT INTO `daftar_buku` (kode_buku, judul_buku, penulis_buku) VALUE(null, '" + judul + "','" + penulis + "')";
      update(query);
    } catch (Exception e) {
      System.out.println(e.toString());
    }
  }

  //mengedit data yang ada di database
  public void editBuku(String judul, String penulis) {
    try {
      String query = "UPDATE `daftar_buku` SET `judul_buku` = '" + judul + "',`penulis_buku` =  '" + penulis + "' WHERE  `daftar_buku`;";
      update(query);
    } catch (Exception e) {
      System.out.println(e.toString());
    }
  }
}
