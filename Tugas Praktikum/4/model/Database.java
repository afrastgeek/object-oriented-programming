package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class Database {
  private String connectionString =
    "jdbc:mysql://localhost/gui_crud?user=root&password=";
  private Statement statement;
  private ResultSet resultSet;
  public Connection connection;

  public Database() throws Exception, SQLException {
    try {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      connection = DriverManager.getConnection(connectionString);
      // connection
      //   .setTransactionIsolation(connection.TRANSACTION_READ_UNCOMMITTED);
    } catch (SQLException e) {
      throw e;
    }
  }

  public void query(String query) throws Exception, SQLException {
    try {
      statement = connection.createStatement();
      resultSet = statement.executeQuery(query);

      if (statement.execute(query)) resultSet = statement.executeQuery(query);
    } catch (SQLException e) {
      throw e;
    }
  }

  public void update(String query) throws Exception, SQLException {
    try {
      statement = connection.createStatement();
      int result = statement.executeUpdate(query);
    } catch (SQLException e) {
      throw e;
    }
  }

  public ResultSet getResultSet() throws Exception {
    try {
      return resultSet;
    } catch (Exception e) {
      return (ResultSet) null;
    }
  }

  public void closeResultSet() throws SQLException, Exception {
    if (resultSet != null) {
      try {
        resultSet.close();
      } catch (SQLException e) {
        resultSet = null;
        throw e;
      }
    }

    if (statement != null) {
      try {
        statement.close();
      } catch (SQLException e) {
        statement = null;
        throw e;
      }
    }
  }

  public void closeConnection() throws SQLException, Exception {
    if (connection != null) {
      try {
        connection.close();
      } catch (SQLException e) {
        connection = null;
        throw e;
      }
    }
  }
}
