package controller;

import java.io.*;
import model.BookModel;

public class BookPresenter {

  private int jml;
  private String judul;
  private String penulis;
  private String[][] daftar = new String[25][5];

  private String error;

  public BookPresenter() {
    //konstruktor
    this.jml = 0;
  }

  public void prosesBku() {
    jml = 0;
    try {

      BookModel bku = new BookModel();
      bku.getDaftar_Buku();

      while (bku.getResultSet().next()) {

        String judul = bku.getResultSet().getString(2);
        String penulis = bku.getResultSet().getString(3);

        daftar[jml][0] = judul;
        daftar[jml][1] = penulis;
        jml++;
      }
      bku.closeResultSet();
      bku.closeConnection();
    } catch (Exception e) {

      error = e.toString();
    }
  }

  public int getJml() {
    return this.jml;
  }

  public String[][] getHasil() {
    return this.daftar;
  }

  public String getError() {
    return this.error;
  }

  //fungsi untuk menghapus data ke dalam database
  public void hapusbuku(String a) {
    try {

      BookModel bku = new BookModel();
      bku.hapusBuku(a);

      bku.closeResultSet();
      bku.closeConnection();
    } catch (Exception e) {
      error = e.toString();
    }
  }


  //fungsi untuk menambahkan data ke dalam database
  public void tambahBuku(String a, String b) {
    try {

      BookModel bku = new BookModel();
      bku.insertBuku(a, b);

      bku.closeResultSet();
      bku.closeConnection();
    } catch (Exception e) {
      error = e.toString();
    }
  }

  //fungsi untuk mengedit data ke dalam database
  public void editbuku(String a, String b) {
    try {

      BookModel bku = new BookModel();
      bku.insertBuku(a, b);

      bku.closeResultSet();
      bku.closeConnection();
    } catch (Exception e) {
      error = e.toString();
    }
  }
}
