CREATE DATABASE gui_crud;
USE gui_crud;

CREATE TABLE daftar_buku(
	kode_buku int(4) PRIMARY KEY auto_increment,
	judul_buku varchar(50),
	penulis_buku varchar(40)
);

INSERT INTO daftar_buku VALUES
	(NULL,'Mockingjay', 'Suzanne Collins'),
	(NULL,'My Wonderful Wizard of Oz', 'L. Frank Baum'),
	(NULL,'The Boy in the Stripes Pajamas', 'John Boyne'),
	(NULL,'I Am Ok You Are Ok', 'Thomas Harris'),
	(NULL,'Norwegian Wood', 'Haruki Murakami'),
	(NULL,'The Day I Swapped My Dad for Two Goldfish', 'Neil Gaiman'),
	(NULL,'Demian', 'Herman Hesse');
