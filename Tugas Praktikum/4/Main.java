import javafx.application.Application;
import javafx.stage.Stage;
import view.BookView;

public class Main extends Application {
  @Override
  public void start(Stage stage) throws Exception {
    (new BookView()).show(stage);
  }
  public static void main(String[] args) {
    launch(args);
  }
}
