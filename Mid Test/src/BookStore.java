/**
 * BookStore Class
 *
 * @author M Ammar Fadhlur Rahman
 */
public class BookStore extends BookSeller {

  String BookStoreCode;
  String BookStoreName;
  String BookStoreAddress;


  public BookStore(String sellerCode, String sellerName, String sellerAddress, String bookStoreCode, String bookStoreName,
                   String bookStoreAddress) {
    super(sellerCode, sellerName, sellerAddress);
    BookStoreCode = bookStoreCode;
    BookStoreName = bookStoreName;
    BookStoreAddress = bookStoreAddress;
  }

  public String getBookStoreCode() {
    return BookStoreCode;
  }

  public void setBookStoreCode(String bookStoreCode) {
    BookStoreCode = bookStoreCode;
  }

  public String getBookStoreName() {
    return BookStoreName;
  }

  public void setBookStoreName(String bookStoreName) {
    BookStoreName = bookStoreName;
  }

  public String getBookStoreAddress() {
    return BookStoreAddress;
  }

  public void setBookStoreAddress(String bookStoreAddress) {
    BookStoreAddress = bookStoreAddress;
  }

}
