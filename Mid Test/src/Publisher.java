/**
 * Publisher Interface
 *
 * @author M Ammar Fadhlur Rahman
 */
public interface Publisher {

  public String getPublisherCode();
  public void setPublisherCode(String publisherCode);
  public String getPublisherName();
  public void setPublisherName(String publisherName);
  public String getPublisherAddress();
  public void setPublisherAddress(String publisherAddress);

}
