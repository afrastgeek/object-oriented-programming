/**
 * Book Class
 *
 * @author M Ammar Fadhlur Rahman
 */
public class Book extends BookStore implements BookGenreCategory, Publisher {

  String BookCode;
  String BookTitle;
  String BookGenreCode;
  String BookGenreName;
  String BookGenreDescription;
  String PublisherCode;
  String PublisherName;
  String PublisherAddress;

  public Book(String bookCode, String bookTitle, String bookGenreCode, String bookGenreName, String bookGenreDescription,
              String publisherCode, String publisherName, String publisherAddress, String sellerCode, String sellerName,
              String sellerAddress, String bookStoreCode, String bookStoreName, String bookStoreAddress) {
    super(sellerCode, sellerName, sellerAddress, bookStoreCode, bookStoreName, bookStoreAddress);
    BookCode = bookCode;
    BookTitle = bookTitle;
    BookGenreCode = bookGenreCode;
    BookGenreName = bookGenreName;
    BookGenreDescription = bookGenreDescription;
    PublisherCode = publisherCode;
    PublisherName = publisherName;
    PublisherAddress = publisherAddress;
  }

  public String getBookCode() {
    return BookCode;
  }

  public void setBookCode(String bookCode) {
    BookCode = bookCode;
  }

  public String getBookTitle() {
    return BookTitle;
  }

  public void setBookTitle(String bookTitle) {
    BookTitle = bookTitle;
  }

  public String getBookGenreCode() {
    return BookGenreCode;
  }

  public void setBookGenreCode(String bookGenreCode) {
    BookGenreCode = bookGenreCode;
  }

  public String getBookGenreName() {
    return BookGenreName;
  }

  public void setBookGenreName(String bookGenreName) {
    BookGenreName = bookGenreName;
  }

  public String getBookGenreDescription() {
    return BookGenreDescription;
  }

  public void setBookGenreDescription(String bookGenreDescription) {
    BookGenreDescription = bookGenreDescription;
  }

  public String getPublisherCode() {
    return PublisherCode;
  }

  public void setPublisherCode(String publisherCode) {
    PublisherCode = publisherCode;
  }

  public String getPublisherName() {
    return PublisherName;
  }

  public void setPublisherName(String publisherName) {
    PublisherName = publisherName;
  }

  public String getPublisherAddress() {
    return PublisherAddress;
  }

  public void setPublisherAddress(String publisherAddress) {
    PublisherAddress = publisherAddress;
  }

}
