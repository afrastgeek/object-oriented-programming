/**
 * BookSeller Class
 *
 * @author M Ammar Fadhlur Rahman
 */
public class BookSeller {
  String SellerCode;
  String SellerName;
  String SellerAddress;

  public BookSeller(String sellerCode, String sellerName, String sellerAddress) {
    SellerCode = sellerCode;
    SellerName = sellerName;
    SellerAddress = sellerAddress;
  }

  public String getSellerCode() {
    return SellerCode;
  }

  public void setSellerCode(String sellerCode) {
    SellerCode = sellerCode;
  }

  public String getSellerName() {
    return SellerName;
  }

  public void setSellerName(String sellerName) {
    SellerName = sellerName;
  }

  public String getSellerAddress() {
    return SellerAddress;
  }

  public void setSellerAddress(String sellerAddress) {
    SellerAddress = sellerAddress;
  }

  public BookSeller() {
    // TODO Auto-generated constructor stub
  }

}
