/**
 * BookGenreCategory Interface
 *
 * @author M Ammar Fadhlur Rahman
 */
public interface BookGenreCategory {

  public String getBookGenreCode();
  public void setBookGenreCode(String bookGenreCode);
  public String getBookGenreName();
  public void setBookGenreName(String bookGenreName);
  public String getBookGenreDescription();
  public void setBookGenreDescription(String bookGenreDescription);

}
