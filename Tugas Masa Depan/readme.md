# Catch The Goals Please


## Running the Application
For easier compilation, I use [Gradle](https://gradle.org/) to build the
application.

This will download the required dependency, compile the source file, and run
the application.

**On Windows:**

```
gradle run -q
```

**On Linux:**

```
./gradle run -q
```