package controller;

import common.Goal;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.Scene;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import view.PlayView;

/**
 * Controls the data flow into a play object and updates the view whenever data
 * changes.
 */
public class PlayController {
  private final Stage primaryStage;
  private final PlayView view = new PlayView(this);
  private final Image stand = new Image("resources/frame-2.png");
  private final Image jump = new Image("resources/jump-up.png");
  private final Image fall = new Image("resources/jump-fall.png");


  /**
   * Constructs the object.
   *
   * @param      primaryStage  The primary stage
   */
  public PlayController(final Stage primaryStage) {
    this.primaryStage = primaryStage;
  }


  /**
   * Gets the primary stage.
   *
   * @return     The primary stage.
   */
  public Stage getPrimaryStage() {
    return primaryStage;
  }


  /**
   * Gets the view.
   *
   * @return     The view.
   */
  public PlayView getView() {
    return view;
  }


  /**
   * Keyboard Key Event Handler
   *
   * @param      scene  The scene
   */
  public void handleKeyEvent(Scene scene) {
    scene.setOnKeyPressed(event -> {
      switch (event.getCode()) {
        case SPACE:
          view.makeUnrunnable();
          System.out.println("SPACE key pressed, switching to Welcome Scene...");
          final WelcomeController welcomeController = new WelcomeController(primaryStage);
          final Scene welcomeScene = new Scene(welcomeController.getView());
          primaryStage.setScene(welcomeScene);
        break;
        case UP:
          System.out.println("UP key pressed, make him jump!");
          view.setPlayerImage(new Image("resources/jump-up.png"));
          if (!view.getFall()) {
            view.makeJump();
          }
        break;
      }
    });
  }

  /**
   * Sinusoidal easing out formula
   *
   * @param      t     time or position elapsed
   *
   * @return     double easing out value
   */
  private double easeOutSine (double t) {
    return 1.0d * Math.sin(t/1.0d * (Math.PI/2)) + 0.8d;
  };

  /**
   * Draws a player.
   */
  public void drawPlayer() {
    ImageView playerImage = view.getPlayerImage();
    Rectangle player = view.getPlayer();
    if(view.getJump()) {
      view.setPlayerImage(jump);
      if (player.getY() >= 0) {
        playerImage.setY(player.getY() - easeOutSine(player.getY() / 276));
        player.setY(playerImage.getY());
      } else {
        view.makeStand();
      }
    } else {
      view.makeFall(true);
      view.setPlayerImage(fall);
      if (player.getY() <= 360 - 84) {
        playerImage.setY(player.getY() + easeOutSine(player.getY() / 276));
        player.setY(playerImage.getY());
      } else {
        view.makeFall(false);
        view.setPlayerImage(stand);
      }
    }
  }

  public void spawnGoal(boolean random) {
    final Random rnd = new Random();

    if(random && rnd.nextInt(350) != 0) {
      return;
    }

    Goal goal = new Goal();

    // y position range: enemy is always on top of player.
    double y = rnd.nextDouble() * (360 - 84 - goal.getRadiusY());
    goal.setCenterY(y);

    // x position: right beside the view, so that it becomes visible with the next game iteration
    double x = goal.getSpeed() < 0 ? x = 640 + goal.getRadiusX() : -goal.getRadiusX();
    goal.setCenterX(x);

    getView().getChildren().add(goal);
    (view.getGoals()).add(goal);
  }

  public void drawGoal() {
    List<Goal> goals = view.getGoals();
    Iterator<Goal> i = goals.iterator();
    while(i.hasNext()) {
      Goal goal = i.next();
      if (goal.getSpeed() > 0 && goal.getCenterX() <= 640 + 30) {
        goal.setCenterX(goal.getCenterX() + goal.getSpeed());
      } else if (goal.getSpeed() < 0 && goal.getCenterX() >= 0 - 30) {
        goal.setCenterX(goal.getCenterX() + goal.getSpeed());
      }

      if (goal.getBoundsInLocal().intersects(view.getPlayer().getBoundsInLocal())) {
        view.getChildren().remove(goal);
        if (goal.getValue() == 0) {
          final WelcomeController welcomeController = new WelcomeController(primaryStage);
          final Scene welcomeScene = new Scene(welcomeController.getView());
          primaryStage.setScene(welcomeScene);
          i.remove();
        }
      }
    }
  }
}
