package controller;

import controller.PlayController;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.WelcomeView;

/**
 * Controls the data flow into a welcome object and updates the view whenever
 * data changes.
 */
public class WelcomeController implements EventHandler {
  private final Stage primaryStage;
  private final WelcomeView view = new WelcomeView(this);


  /**
   * Constructs the object.
   *
   * @param      primaryStage  The primary stage
   */
  public WelcomeController(final Stage primaryStage) {
    this.primaryStage = primaryStage;
  }


  /**
   * Event Handler
   *
   * @param      event  The event
   */
  @Override
  public void handle(final Event event) {
    final Object source = event.getSource();

    if (source.equals(view.getPlayButton())) {
      System.out.println("playButton pressed, switching to Play Scene...");

      final PlayController playController = new PlayController(primaryStage);
      final Scene scene = new Scene(playController.getView());
      playController.getView().makeRunnable();
      playController.handleKeyEvent(scene);
      primaryStage.setScene(scene);
    }

    if (source.equals(view.getExitButton())) {
      System.out.println("exitButton pressed, exiting...");
      System.exit(0);
    }
  }


  /**
   * Gets the primary stage.
   *
   * @return     The primary stage.
   */
  public Stage getPrimaryStage() {
    return primaryStage;
  }


  /**
   * Gets the view.
   *
   * @return     The view.
   */
  public WelcomeView getView() {
    return view;
  }

}
