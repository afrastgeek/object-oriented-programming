package view;

import controller.WelcomeController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Main Menu visualization.
 */
public class WelcomeView extends VBox {
  private final Text gameTitle = new Text("Catch The Goal Please");
  private final Label usernameLabel = new Label("Username : ");
  private final TextField usernameTextField = new TextField();
  private final Button playButton = new Button("Main");
  private final Button exitButton = new Button("Keluar");
  private final HBox mainControl = new HBox(
    usernameLabel, usernameTextField, playButton, exitButton
  );
  private final ListView scoreListView = new ListView();
  private final HBox scoreBoard = new HBox(scoreListView);


  /**
   * Constructs the object.
   *
   * @param      welcomeController  The welcome controller
   */
  public WelcomeView(final WelcomeController welcomeController) {
    this.setPrefSize(640,360);
    this.setAlignment(Pos.CENTER);
    this.setStyle("-fx-background-color: white;");
    this.getChildren().addAll(gameTitle, mainControl, scoreBoard);

    mainControl.setAlignment(Pos.CENTER);
    mainControl.setSpacing(10);
    mainControl.setPadding(new Insets(20, 0, 0, 0));

    scoreBoard.setAlignment(Pos.CENTER);
    scoreBoard.setPadding(new Insets(20, 0, 40, 0));

    playButton.setOnAction(welcomeController);
    exitButton.setOnAction(welcomeController);
  }


  /**
   * Gets the play button.
   *
   * @return     The play button.
   */
  public Button getPlayButton() {
    return playButton;
  }


  /**
   * Gets the exit button.
   *
   * @return     The exit button.
   */
  public Button getExitButton() {
    return exitButton;
  }

}
