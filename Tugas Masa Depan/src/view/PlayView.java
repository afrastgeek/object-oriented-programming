package view;

import common.Goal;
import controller.PlayController;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

/**
 * Play visualization of the Catch the Goal Please game.
 */
public class PlayView extends Pane {
  public static boolean RUN = true;
  public static boolean JUMP = false;
  public static boolean FALL = false;
  public static Rectangle player = new Rectangle();
  public ImageView playerImage = new ImageView();
  List<Goal> goals = new ArrayList<>();


  /**
   * Constructs the object.
   *
   * @param      playController  The play controller
   */
  public PlayView(final PlayController playController) {
    this.setPrefSize(640,360);
    this.setStyle("-fx-background-color: white;");
    getChildren().add(playerImage);
    playerImage.setFitWidth(60);
    playerImage.setPreserveRatio(true);
    setPlayerImage(new Image("resources/frame-2.png"));
    playerImage.setX(290);
    playerImage.setY(360-84);
    player.setWidth(60);
    player.setHeight(84);
    player.setX(playerImage.getX());
    player.setY(playerImage.getY());

    new Thread(new Runnable() {
      @Override public void run() {
        while (RUN) {
          try {
            Thread.sleep(3);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          Platform.runLater(new Runnable() {
            @Override public void run() {
              playController.drawPlayer();
              playController.spawnGoal(true);
              playController.drawGoal();
            }
          });
        }
      }
    }).start();
  }

  /**
   * Makes runnable runnable again.
   */
  public void makeRunnable() {
    RUN = true;
  }

  /**
   * Makes runnable unrunnable.
   */
  public void makeUnrunnable() {
    RUN = false;
  }

  /**
   * Gets the jump value.
   *
   * @return     The jump.
   */
  public boolean getJump() {
    return JUMP;
  }

  /**
   * Makes player jump.
   */
  public void makeJump() {
    JUMP = true;
  }

  /**
   * Makes player stand.
   */
  public void makeStand() {
    JUMP = false;
  }

  /**
   * Gets the fall value.
   *
   * @return     The fall.
   */
  public boolean getFall() {
    return FALL;
  }

  /**
   * Makes player fall.
   */
  public void makeFall(boolean state) {
    FALL = state;
  }


  /**
   * Gets the player.
   *
   * @return     The player.
   */
  public Rectangle getPlayer() {
    return player;
  }


  /**
   * Gets the player image.
   *
   * @return     The player image.
   */
  public ImageView getPlayerImage() {
    return playerImage;
  }

  /**
   * Sets the player image.
   *
   * @param      image  The image
   */
  public void setPlayerImage(Image image) {
    playerImage.setImage(image);
  }

  public List<Goal> getGoals() {
    return goals;
  }

}
