import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import controller.WelcomeController;

/**
* Class for main.
*/
public class Main extends Application {
  public static void main(final String[] args) {
    launch();
  }


  /**
  * Launch Application
  *
  * @param      primaryStage  The primary stage
  *
  * @throws     Exception     Program Exception
  */
  @Override
  public void start(final Stage primaryStage) throws Exception {
    final Scene scene = new Scene(
      new WelcomeController(primaryStage).getView()
    );

    primaryStage.setScene(scene);
    primaryStage.setTitle("Catch The Goal Please");;
    primaryStage.show();
  }

  /**
   * Force Java virtual machine to exit, killing background thread.
   */
  @Override
  public void stop(){
    System.out.println("Stage is closing");
    System.exit(0);
  }
}
