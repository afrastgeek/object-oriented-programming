package common;

import java.util.Random;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;

/**
 * Class for goal.
 */
public class Goal extends Ellipse {
  private int value;
  private double speed;
  private Random rnd = new Random();

  /**
   * Constructs the object.
   */
  public Goal() {
    this.setRadiusX(30);
    this.setRadiusY(20);

    // random speed
    speed = rnd.nextDouble() * 2d - 1d;
    if (speed < 0.3d) {
      if (speed > -0.3d) {
        speed = 0.3;
      }
    }

    value = (new Random()).nextInt(51);
    value = value < 10 ? 0 : value;

    if (value != 0) {
      this.setFill(new Color(
        rnd.nextFloat() / 2f + 0.5f,
        rnd.nextFloat() / 2f + 0.5f,
        rnd.nextFloat() / 2f + 0.5f,
        1.0
      ));
    } else {
      this.setFill(Color.BLACK);
    }
  }

  /**
   * Gets the value.
   *
   * @return     The value.
   */
  public int getValue() {
    return value;
  }

  /**
   * Gets the speed.
   *
   * @return     The speed.
   */
  public double getSpeed() {
    return speed;
  }

}
